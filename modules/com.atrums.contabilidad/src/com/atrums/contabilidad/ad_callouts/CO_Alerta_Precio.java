package com.atrums.contabilidad.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class CO_Alerta_Precio extends HttpSecureAppServlet {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	public void init(ServletConfig config) {
		super.init(config);
		boolHist = false;
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		VariablesSecureApp vars = new VariablesSecureApp(request);

		if (vars.commandIn("DEFAULT")) {
			String strIsSoTrx = vars.getStringParameter("inpissotrx");
			String strMProductId = vars.getStringParameter("inpmProductId");
			String strPriceactual = vars.getStringParameter("inppriceactual");

			try {
				printPage(response, vars, strMProductId, strPriceactual, strIsSoTrx);
			} catch (ServletException ex) {
				pageErrorCallOut(response);
			}
		} else
			pageError(response);
	}

	private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strMProductId,
			String strPriceactual, String strIsSoTrx) throws IOException, ServletException {
		log4j.debug("Output: dataSheet");

		XmlDocument xmlDocument = xmlEngine.readXmlTemplate("com/atrums/contabilidad/ad_callouts/CallOut")
				.createXmlDocument();

		StringBuffer result = new StringBuffer();
		String mensaje = "";

		String precio = strPriceactual.replaceAll(",", "");
		float floprecio = Float.parseFloat(precio);

		if (strMProductId.equals("")) {
			mensaje = "";
		} else {
			if (floprecio > 0.0f) {
				if (strIsSoTrx.equals("Y")) {
					mensaje = COAlertaPrecioData.select(this, strMProductId, strPriceactual);
				}
			}
		}

		if (!mensaje.equals("")) {
			String strConvRateErrorMsg = "";
			String cadena = "Advertencia: ";

			strConvRateErrorMsg = cadena + mensaje;
			// FIN
			result.append("var calloutName='CO_Alerta_Precio';\n\n");
			result.append("var respuesta = new Array(new Array(\"MESSAGE\", ");
			result.append("\"" + " " + strConvRateErrorMsg + "\")");

			// result.append("new Array(\"inppriceactual\", \"" + "" + "\")");
			result.append(");");

		} else {
			result.append("var calloutName='CO_Alerta_Precio';\n\n");
			result.append("var respuesta = new Array(");
			// result.append("new Array(\"inppriceactual\", \"" + strPriceactual.trim() +
			// "\")");
			result.append(");");
		}

		// inject the generated code
		xmlDocument.setParameter("array", result.toString());

		xmlDocument.setParameter("frameName", "appFrame");

		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.println(xmlDocument.print());
		out.close();
	}
}
