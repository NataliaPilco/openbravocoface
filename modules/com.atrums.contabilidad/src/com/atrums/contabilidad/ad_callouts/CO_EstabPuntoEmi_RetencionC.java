package com.atrums.contabilidad.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class CO_EstabPuntoEmi_RetencionC extends HttpSecureAppServlet
{
   /**
   * 
   */
    private static final long serialVersionUID = 1L;

    public void init(ServletConfig config)
    {
	super.init(config);
	boolHist = false;
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException
    {
	VariablesSecureApp vars = new VariablesSecureApp(request);

	if (vars.commandIn("DEFAULT"))
	{
	    String strOrgId = vars.getStringParameter("inpadOrgId");

	    try
	    {
		printPage(response, vars, strOrgId);
	    }
	    catch (ServletException ex)
	    {
		pageErrorCallOut(response);
	    }
	}
	else
	    pageError(response);
    }

    private void printPage(HttpServletResponse response,
	    VariablesSecureApp vars, String strOrgId)
	    throws IOException, ServletException
    {
	log4j.debug("Output: dataSheet");

	XmlDocument xmlDocument = xmlEngine
		.readXmlTemplate("com/atrums/contabilidad/ad_callouts/CallOut")
		.createXmlDocument();

	StringBuffer result = new StringBuffer();

	result.append("var calloutName='CO_EstabPuntoEmi_RetencionC';\n\n");
	result.append("var respuesta = new Array(");
	
	COEstabPuntoEmiRetencionCData[] data = COEstabPuntoEmiRetencionCData
		    .select(this, strOrgId);

	    result.append("new Array(\"inpemAtecfeNroEstab\", \""
		    + data[0].getField("establecimiento") + "\"),");
	    result.append("new Array(\"inpemAtecfePuntoEmision\", \""
		    + data[0].getField("emision") + "\")");
	
	result.append(");");

	// inject the generated code
	xmlDocument.setParameter("array", result.toString());

	xmlDocument.setParameter("frameName", "appFrame");

	response.setContentType("text/html; charset=UTF-8");
	PrintWriter out = response.getWriter();
	out.println(xmlDocument.print());
	out.close();
    }
}
