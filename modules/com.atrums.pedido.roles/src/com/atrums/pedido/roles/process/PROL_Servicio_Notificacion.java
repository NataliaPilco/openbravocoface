package com.atrums.pedido.roles.process;


import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.scheduling.Process;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.model.common.order.Order;

/**
 * @author ATRUMS-IT
 *
 */
public class PROL_Servicio_Notificacion implements Process {
  static Logger log4j = Logger.getLogger(PROL_Servicio_Notificacion.class);
  final OBError msg = new OBError();

  @Override
  public void execute(ProcessBundle bundle) throws Exception {
    // TODO Auto-generated method stub
    ConnectionProvider conn = bundle.getConnection();
    VariablesSecureApp varsAux = bundle.getContext().toVars();

    OBContext.setOBContext(varsAux.getUser(), varsAux.getRole(), varsAux.getClient(),
        varsAux.getOrg());

    ProcesarPedidoData[] prefactura;
    Order orderDato = null;

    try {
    	prefactura = ProcesarPedidoData.methodSeleccionarEnvio(conn,
          varsAux.getClient());

      if (prefactura.length > 0) {

          orderDato = OBDal.getInstance().get(Order.class, prefactura[0].dato1);

          PROL_EnviarEmailNotificacion.procesoEnviar(orderDato, conn, "100", "Enviado", msg);
          ProcesarPedidoData.methodActualizaEnviar(conn, orderDato.getId());
         
      }
    } catch (Exception ex) {
      Log.error(ex.getMessage(), ex);
        //ATECOFFGenerarXmlData.methodActualizarInvEstadoOffline(conn, "PD", id);    
    } finally {
      // TODO: handle finally clause
    	prefactura = null;
    	orderDato = null;
    	OBDal.getInstance().rollbackAndClose();
    }
  }

}
