package com.atrums.pedido.roles.process;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.common.order.Order;

import com.atrums.offline.feletronica.process.ATECOFF_Funciones_Aux;
import com.atrums.offline.feletronica.process.ATECOFF_Operacion_Auxiliares;


public class PROL_EnviarEmailNotificacion {
	
	public static boolean procesoEnviar(Order orderDato, ConnectionProvider conn, String strUser,
		      String string, OBError msg) throws Exception {


		byte[] btyDoc = null;
		ProcesarPedidoData[] emailData;
		File flPdf = null;
		String strContenido = "";
		String strAsunto = "";
		
		strAsunto = "NOTIFICACION ATRUMS ERP";

		ATECOFF_Funciones_Aux opeaux = new ATECOFF_Funciones_Aux();
		emailData = ProcesarPedidoData.methodEnviarDatos(conn, orderDato.getId());
				
		strContenido = "Estimados Sres/Sras." + 
		        "\r\n\nEl usuario " + emailData[0].dato4 + ", ha generado una solicitud de facturación (Prefactura) N° " + emailData[0].dato1 + ", de la empresa " + emailData[0].dato8 + ", a nombre de " + emailData[0].dato3 + ", por un monto total de $ " + emailData[0].dato5 + ". \r\n" + 
		        "\r\n" + 
				"Estado del Documento: " + emailData[0].dato7 + "\r\n" +
		        "\r\n" + 
				"Adjunto al presente correo encontrarán el documento generado.\r\n" + 
				"\r\n" + 
				"La presente notificación se ha generado de manera automática, por favor, no responda a este mensaje.\r\n" + 
				"\r\n\n" + 
				"Saludos cordiales";
		
		flPdf = ATECOFF_Operacion_Auxiliares.generarPDF(conn,
				"@basedesign@/com/atrums/pedido/roles/erpReport/Rpt_PreFactura.jrxml", "Pre_Factura", orderDato.getId());
	 
		
		if (emailData.length > 0) {
			btyDoc = emailData[0].dato1.getBytes();
		}

		File flDocXML = null;
		List<File> lisdoc = new ArrayList<File>();

		if (btyDoc != null) {

			flDocXML = opeaux.bytetofile(btyDoc);
			if (flDocXML != null) {
			
				//lisdoc.add(flDocXML);
				lisdoc.add(flPdf);
				
			}
		}
		
		if (ATECOFF_Operacion_Auxiliares.enviarCorreo(emailData[0].dato6.toString(), strAsunto,
				strContenido, null, lisdoc, false)) {
			flDocXML.delete();
			flPdf.delete();
		    return true;
		}else {
			return false;
		} 
		
	}


}
