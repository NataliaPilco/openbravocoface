package com.atrums.pedido.roles.model;

public class PROL_Temporal {

  double amount;
  String referenceno;
  String nro_cheque;
  String fin_paymentmethod_id;
  String c_order_id;
  String banco_id;
  String tarjeta_id;
  String condicion_id;
  String tipoPago;
  String fin_payment_id;

  public PROL_Temporal() {
    super();
  }

  public String getCondicion_id() {
    return condicion_id;
  }

  public void setCondicion_id(String condicion_id) {
    this.condicion_id = condicion_id;
  }

  public String getFin_payment_id() {
    return fin_payment_id;
  }

  public void setFin_payment_id(String fin_payment_id) {
    this.fin_payment_id = fin_payment_id;
  }

  public String getTipoPago() {
    return tipoPago;
  }

  public void setTipoPago(String tipoPago) {
    this.tipoPago = tipoPago;
  }

  public String getBanco_id() {
    return banco_id;
  }

  public void setBanco_id(String banco_id) {
    this.banco_id = banco_id;
  }

  public String getTarjeta_id() {
    return tarjeta_id;
  }

  public void setTarjeta_id(String tarjeta_id) {
    this.tarjeta_id = tarjeta_id;
  }

  public double getAmount() {
    return amount;
  }

  public void setAmount(double amount) {
    this.amount = amount;
  }

  public String getReferenceno() {
    return referenceno;
  }

  public void setReferenceno(String referenceno) {
    this.referenceno = referenceno;
  }

  public String getNro_cheque() {
    return nro_cheque;
  }

  public void setNro_cheque(String nro_cheque) {
    this.nro_cheque = nro_cheque;
  }

  public String getFin_paymentmethod_id() {
    return fin_paymentmethod_id;
  }

  public void setFin_paymentmethod_id(String fin_paymentmethod_id) {
    this.fin_paymentmethod_id = fin_paymentmethod_id;
  }

  public String getC_order_id() {
    return c_order_id;
  }

  public void setC_order_id(String c_order_id) {
    this.c_order_id = c_order_id;
  }

}
