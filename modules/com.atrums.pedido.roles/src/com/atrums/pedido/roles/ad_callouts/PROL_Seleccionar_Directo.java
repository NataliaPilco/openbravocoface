package com.atrums.pedido.roles.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class PROL_Seleccionar_Directo extends HttpSecureAppServlet {

  /**
  * 
  */
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strbroker = vars.getStringParameter("inpemProlBroker");
      String strdirecto = vars.getStringParameter("inpemProlDirecto");
      String strcatalogrobrokerid = vars.getStringParameter("inpemProlCatalogoBrokersId");

      try {
          printPage(response, vars, strbroker, strcatalogrobrokerid, strdirecto);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strbroker, String strcatalogrobrokerid, String strdirecto) throws IOException, ServletException {
    log4j.debug("Output: dataSheet");

    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate("com/atrums/pedido/roles/ad_callouts/CallOut").createXmlDocument();

    StringBuffer result = new StringBuffer();
    
    result.append("var calloutName='PROL_Seleccionar_Broker';\n\n");
    
    if (strdirecto.equals("Y")) {
	    result.append("var respuesta = new Array(new Array(\"inpemProlBroker\", \"" + "N" + "\"),");
	    result.append("new Array(\"inpemProlCatalogoBrokersId\", \"" + "" + "\")");
	    result.append(");");
    }	


    // inject the generated code
    xmlDocument.setParameter("array", result.toString());

    xmlDocument.setParameter("frameName", "appFrame");

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

}
