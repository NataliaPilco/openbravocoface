<?xml version="1.0" encoding="UTF-8" ?>
<!--
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2010 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
-->

<SqlClass name="PocData" package="com.atrums.reporting.utility.reporting.printing">
   <SqlClassComment></SqlClassComment>
   
   <!-- 
		This definition exists only to define all the fields used in all the queries.
		This is needed because all the variables in the generated class are based upon
		all the fields in the first query of the xsql.
    -->
   <SqlMethod name="dummy" type="preparedStatement" return="multiple">
      <SqlMethodComment></SqlMethodComment>
      <Sql>
      <![CDATA[
		select
			'' as document_id,
			'' as docstatus,
			'' as ourreference,
			'' as yourreference,
			'' as salesrep_user_id,
			'' as salesrep_email,
			'' as salesrep_name,
			'' as bpartner_id,
			'' as bpartner_name,
			'' as contact_user_id,
			'' as contact_email,
			'' as contact_name,
			'' as ad_user_id,
			'' as user_email,
			'' as user_name,
			'' as report_location
		from
			dual
        ]]>
        </Sql>
   </SqlMethod>
   
   <SqlMethod name="getContactDetailsForRetenciones" type="preparedStatement" return="multiple">
      <SqlMethodComment></SqlMethodComment>
      <Sql>
      <![CDATA[
        select
            co_retencion_compra.co_retencion_compra_id as document_id,
            co_retencion_compra.docstatus as docstatus,
            co_retencion_compra.documentno as ourreference,
            null as yourreference,
            c_invoice.salesrep_id as salesrep_user_id,
            salesrep.email as salesrep_email,
            salesrep.name as salesrep_name,
            c_invoice.c_bpartner_id as bpartner_id,
            c_bpartner.name as bpartner_name,
            c_invoice.ad_user_id as contact_user_id,
            customercontact.email as contact_email,
            customercontact.name as contact_name
        from co_retencion_compra left join c_invoice on co_retencion_compra.c_invoice_id = c_invoice.c_invoice_id
                                 left join ad_user customercontact on c_invoice.ad_user_id = customercontact.ad_user_id
                                 left join ad_user salesrep on c_invoice.salesrep_id = salesrep.ad_user_id
                                 left join c_bpartner on c_invoice.c_bpartner_id = c_bpartner.c_bpartner_id
        where
			1=1
        ]]>
        </Sql>
        <Parameter name="cRetencionCompraId" optional="false" type="argument" after="1=1">
           <![CDATA[ and co_retencion_compra.co_retencion_compra_id in ]]>
        </Parameter>
   </SqlMethod>
   
   <SqlMethod name="getContactDetailsForUser" type="preparedStatement" return="multiple">
      <SqlMethodComment></SqlMethodComment>
      <Sql>
      <![CDATA[
        select
            ad_user.ad_user_id,
            ad_user.email as user_email,
            ad_user.name as user_name
        from
            ad_user
        where
            ad_user.ad_user_id = ?
        ]]>
        </Sql>
        <Parameter name="adUserId"/> 
   </SqlMethod>   
   
      <SqlMethod name="getContactDetailsMormovement" type="preparedStatement" return="multiple">
      <SqlMethodComment></SqlMethodComment>
      <Sql>
      <![CDATA[
         select m_movement.m_movement_id as document_id,
                'CL' as docstatus,
                m_movement.documentno as ourreference,
                null as yourreference,
                '' as salesrep_user_id,
                '' as salesrep_email,
                '' as salesrep_name,
                '' as bpartner_id,
                '' as bpartner_name,
                m_movement.createdby as contact_user_id,
                '' as contact_email,
                '' as contact_name
           from m_movement 
          where 1=1
        ]]>
        </Sql>
        <Parameter name="mMovementId" optional="false" type="argument" after="1=1">
           <![CDATA[ and m_movement.m_movement_id in ]]>
        </Parameter>
   </SqlMethod>
   
   <SqlMethod name="getContactDetailsForProductions" type="preparedStatement" return="multiple">
      <SqlMethodComment></SqlMethodComment>
      <Sql>
      <![CDATA[
         select m_production.m_production_id as document_id,
                'CL' as docstatus,
                m_production.em_co_secuencial as ourreference,
                null as yourreference,
                '' as salesrep_user_id,
                '' as salesrep_email,
                '' as salesrep_name,
                '' as bpartner_id,
                '' as bpartner_name,
                m_production.createdby as contact_user_id,
                '' as contact_email,
                '' as contact_name
           from m_production 
          where 1=1
        ]]>
        </Sql>
        <Parameter name="mProductionId" optional="false" type="argument" after="1=1">
           <![CDATA[ and m_production. m_production_id in ]]>
        </Parameter>
   </SqlMethod>
   
</SqlClass>

