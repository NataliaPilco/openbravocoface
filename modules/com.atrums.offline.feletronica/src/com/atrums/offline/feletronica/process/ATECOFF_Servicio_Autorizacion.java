package com.atrums.offline.feletronica.process;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.scheduling.Process;
import org.openbravo.scheduling.ProcessBundle;

import com.atrums.contabilidad.data.CO_Retencion_Compra;

/**
 * @author ATRUMS-IT
 *
 */
public class ATECOFF_Servicio_Autorizacion implements Process {
  static Logger log4j = Logger.getLogger(ATECOFF_Servicio_Autorizacion.class);
  final OBError msg = new OBError();

  @Override
  public void execute(ProcessBundle bundle) throws Exception {
    // TODO Auto-generated method stub
    ConnectionProvider conn = bundle.getConnection();
    VariablesSecureApp varsAux = bundle.getContext().toVars();

    OBContext.setOBContext(varsAux.getUser(), varsAux.getRole(), varsAux.getClient(),
        varsAux.getOrg());

    ATECOFFGenerarXmlData[] axmldocumentos;
    Invoice invDato = null;
    CO_Retencion_Compra crcDato = null;

    String tipo = null;
    String id = null;

    try {
      axmldocumentos = ATECOFFGenerarXmlData.methodSeleccionarDocumentoPendientes(conn,
          varsAux.getClient());

      if (axmldocumentos.length > 0) {

        tipo = axmldocumentos[0].dato1;
        id = axmldocumentos[0].dato2;

        if (axmldocumentos[0].dato1.equals("FV")) {
          ATECOFFGenerarXmlData.methodActualizarInvEstadoOffline(conn, "TR",
              axmldocumentos[0].dato2);

          invDato = OBDal.getInstance().get(Invoice.class, axmldocumentos[0].dato2);

          ATECOFF_GenerarFacturaXML.generarFacturaXML(invDato, conn, varsAux.getUser(), true,
              this.msg);
        } else if (axmldocumentos[0].dato1.equals("LC")) {
            ATECOFFGenerarXmlData.methodActualizarInvEstadoOffline(conn, "TR",
                    axmldocumentos[0].dato2);

                invDato = OBDal.getInstance().get(Invoice.class, axmldocumentos[0].dato2);

                ATECOFF_GenerarLiquidacionXML.generarFacturaXMLLiq(invDato, conn, varsAux.getUser(), true,
                    this.msg);
        } else if (axmldocumentos[0].dato1.equals("RT")) {
          ATECOFFGenerarXmlData.methodActualizarRetEstadoOffline(conn, "TR",
              axmldocumentos[0].dato2);

          crcDato = OBDal.getInstance().get(CO_Retencion_Compra.class, axmldocumentos[0].dato2);

          ATECOFF_GenerarRetencionXML.generarFacturaXMLRet(crcDato, conn, varsAux.getUser(), true,
              this.msg);
        } else if (axmldocumentos[0].dato1.equals("GD")) {
          ATECOFFGenerarXmlData.methodActualizarGuiEstadoOffline(conn, "TR",
              axmldocumentos[0].dato2);

          ShipmentInOut spiDato = OBDal.getInstance().get(ShipmentInOut.class,
              axmldocumentos[0].dato2);

          ATECOFF_GenerarGuiaDespachoXML.generarFacturaXMLGre(spiDato, conn, varsAux.getUser(),
              true, this.msg);
        }

      }
    } catch (Exception ex) {
      Log.error(ex.getMessage(), ex);
      if (tipo.equals("FV")) {
        ATECOFFGenerarXmlData.methodActualizarInvEstadoOffline(conn, "PD", id);
      } else if (tipo.equals("RT")) {
        ATECOFFGenerarXmlData.methodActualizarRetEstadoOffline(conn, "PD", id);
      }
    } finally {
      // TODO: handle finally clause
      axmldocumentos = null;
      invDato = null;
      OBDal.getInstance().rollbackAndClose();
    }
  }

}
