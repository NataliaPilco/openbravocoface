package com.atrums.offline.feletronica.process;

public class ATECOFF_ServiceAutorizacion {
  // private static Logger log = Logger.getLogger(ATECOFF_ServiceAutorizacion.class);
  private String ambiente;
  private String claveAcceso;

  public ATECOFF_ServiceAutorizacion(String ambiente, String claveAcceso) {
    this.ambiente = ambiente;
    this.claveAcceso = claveAcceso;
  }

  public ATECOFF_SRIDocumentoAutorizado CallAutorizado() {
    ATECOFF_ServicioSRICall sriCall = new ATECOFF_ServicioSRICall(this.ambiente, null,
        this.claveAcceso);

    ATECOFF_SRIDocumentoAutorizado autorizado = new ATECOFF_SRIDocumentoAutorizado(
        sriCall.AutorizadoCall());

    sriCall = null;

    return autorizado;
  }
}
