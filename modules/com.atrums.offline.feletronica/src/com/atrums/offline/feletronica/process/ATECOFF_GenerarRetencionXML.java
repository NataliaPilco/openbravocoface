package com.atrums.offline.feletronica.process;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.OrganizationInformation;
import org.openbravo.model.common.invoice.Invoice;

import com.atrums.contabilidad.data.CO_Retencion_Compra;

public class ATECOFF_GenerarRetencionXML {

  public static boolean generarFacturaXMLRet(CO_Retencion_Compra crcDato, ConnectionProvider conn,
      String strUser, boolean enviarSRI, OBError msg) throws Exception {
    File flXml = null;
    String fileString = null;
    Document docXML = null;
    Client cltDato = null;
    OutputFormat ofFormat = null;
    DocumentType dctDato = null;
    OrganizationInformation oriDato = null;
    BusinessPartner bspDato = null;
    Invoice invDatoRete = null;
    Organization oriDatoAux = null;
    ATECOFFGenerarXmlData[] axmlDirMatriz = null;
    ATECOFFGenerarXmlData[] axmlDirec = null;
    ATECOFFGenerarXmlData[] axmlImpuestos = null;
    ATECOFFGenerarXmlData[] axmlEmail = null;
    String type = null;

    String emailSoporte = "soporte@atrums.com";

    Hashtable<String, String> hstClaveAcceso = new Hashtable<String, String>();

    try {
      flXml = File.createTempFile("documento", ".xml", null);
      flXml.deleteOnExit();

      docXML = DocumentHelper.createDocument();

      ofFormat = OutputFormat.createPrettyPrint();

      final Element elmret = docXML.addElement("comprobanteRetencion");
      elmret.addAttribute("id", "comprobante");
      elmret.addAttribute("version", "1.0.0");

      final Element elminftri = elmret.addElement("infoTributaria");

      if (crcDato.getClient() != null) {
        cltDato = OBDal.getInstance().get(Client.class, crcDato.getClient().getId());

        Date cldFechaIn = crcDato.getFechaEmisin();
        SimpleDateFormat sdfFormato = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdfFormatoClave = new SimpleDateFormat("ddMMyyyy");
        SimpleDateFormat sdfFormatoPerido = new SimpleDateFormat("MM/yyyy");

        dctDato = OBDal.getInstance().get(DocumentType.class, crcDato.getDocumentType().getId());

        oriDato = OBDal.getInstance().get(OrganizationInformation.class,
            crcDato.getOrganization().getId());

        bspDato = OBDal.getInstance().get(BusinessPartner.class, crcDato.getBpartner().getId());

        axmlDirMatriz = ATECOFFGenerarXmlData.methodSelDirMatriz(conn, crcDato.getClient().getId());

        String strDirMat = "";
        if (axmlDirMatriz != null && axmlDirMatriz.length == 1) {
          strDirMat = axmlDirMatriz[0].dato1;
        }
        
        /********************* 26/05/2021 *********************/
        String strRegimen = "";
        if (axmlDirMatriz != null && axmlDirMatriz.length == 1) {
        	strRegimen = axmlDirMatriz[0].dato2;
        }
        
        String strAgente = "";
        if (axmlDirMatriz != null && axmlDirMatriz.length == 1) {
        	strAgente = axmlDirMatriz[0].dato3;
        }
        /*****************************************************/


        axmlDirec = ATECOFFGenerarXmlData.methodSeleccionarDirec(conn,
            crcDato.getOrganization().getId());

        String strDir = "";
        if (axmlDirec != null && axmlDirec.length == 1) {
          strDir = axmlDirec[0].dato1;
        }

        //invDatoRete = OBDal.getInstance().get(Invoice.class, crcDato.getInvoice().getId());

        oriDatoAux = OBDal.getInstance().get(Organization.class, crcDato.getOrganization().getId());

        if (ATECOFF_Operacion_Auxiliares.generarCabecera(elminftri, cltDato.getAtecfeTipoambiente(),
            dctDato.getCoTipoComprobanteAutorizadorSRI().toString(), oriDato.getTaxID(),
            oriDatoAux.getCoNroEstab(), oriDatoAux.getCoPuntoEmision(), crcDato.getDocumentNo(),
            cltDato.getAtecfeCodinumerico(), cltDato.getAtecfeTipoemisi(),
            sdfFormatoClave.format(cldFechaIn),
            ATECOFF_Operacion_Auxiliares.normalizacionPalabras(strDirMat),
            ATECOFF_Operacion_Auxiliares.normalizacionPalabras(cltDato.getName()),
            ATECOFF_Operacion_Auxiliares.normalizacionPalabras(cltDato.getName()), 
            strRegimen, strAgente, msg,
            hstClaveAcceso) && dctDato != null && oriDato != null && bspDato != null) {
          final Element elmcomret = elmret.addElement("infoCompRetencion");

          elmcomret.addElement("fechaEmision").addText(sdfFormato.format(cldFechaIn));

          if (!strDir.equals("")) {
            elmcomret.addElement("dirEstablecimiento")
                .addText(ATECOFF_Operacion_Auxiliares.normalizacionPalabras(strDir));
          }

          if (cltDato.getAtecfeNumresolsri() != null) {
            String strNumeReso = cltDato.getAtecfeNumresolsri();
            for (int i = 0; i < (3 - cltDato.getAtecfeNumresolsri().length()); i++) {
              strNumeReso = "0" + strNumeReso;
            }

            if (strNumeReso.length() >= 3 && strNumeReso.length() <= 5) {
              elmcomret.addElement("contribuyenteEspecial").addText(strNumeReso);
            } else {
              msg.setType("Error");
              msg.setMessage("El número de contribuyente es de máximo 5 caracteres");
              msg.setTitle("@Error@");
              return false;
            }
          }

          if (cltDato.isAtecfeObligcontabi()) {
            elmcomret.addElement("obligadoContabilidad").addText("SI");
          } else {
            elmcomret.addElement("obligadoContabilidad").addText("NO");
          }

          String strIden = null;

          if (bspDato.getCOTipoIdentificacion().toString().equals("01")
              || bspDato.getCOTipoIdentificacion().toString().equals("1")) {
            elmcomret.addElement("tipoIdentificacionSujetoRetenido").addText("04");
          } else if (bspDato.getCOTipoIdentificacion().toString().equals("02")
              || bspDato.getCOTipoIdentificacion().toString().equals("2")) {
            elmcomret.addElement("tipoIdentificacionSujetoRetenido").addText("05");
          } else if (bspDato.getCOTipoIdentificacion().toString().equals("03")
              || bspDato.getCOTipoIdentificacion().toString().equals("3")) {
            elmcomret.addElement("tipoIdentificacionSujetoRetenido").addText("06"); //JUNIO 2020
          } else if (bspDato.getCOTipoIdentificacion().toString().equals("07")
              || bspDato.getCOTipoIdentificacion().toString().equals("7")) {
            elmcomret.addElement("tipoIdentificacionSujetoRetenido").addText("07");
            strIden = "9999999999999";
          } else {
            msg.setType("Error");
            msg.setMessage("El cliente tiene que tener un tipo de identificacion valido ");
            msg.setTitle("@Error@");
            return false;
          }

          if (bspDato.getName2() != null) {
            elmcomret.addElement("razonSocialSujetoRetenido")
                .addText(ATECOFF_Operacion_Auxiliares.normalizacionPalabras(bspDato.getName2()));
          } else if (bspDato.getName() != null) {
            elmcomret.addElement("razonSocialSujetoRetenido")
                .addText(ATECOFF_Operacion_Auxiliares.normalizacionPalabras(bspDato.getName()));
          } else {
            msg.setType("Error");
            msg.setMessage("Es necesaria la razón social del comprador");
            msg.setTitle("@Error@");
            return false;
          }

          if (bspDato.getTaxID() != null) {

            if (strIden == null) {
              strIden = bspDato.getTaxID();
            }

            elmcomret.addElement("identificacionSujetoRetenido").addText(strIden);
          } else {
            msg.setType("Error");
            msg.setMessage("Es necesaria la CI/RUC/Pasaporte");
            msg.setTitle("@Error@");
            return false;
          }

          if (crcDato.getFechaEmisin() != null) {
            elmcomret.addElement("periodoFiscal")
                .addText(sdfFormatoPerido.format(crcDato.getFechaEmisin()));
          } else {
            msg.setType("Error");
            msg.setMessage("La retencion no está en un periodo fiscal");
            msg.setTitle("@Error@");
            return false;
          }

          final Element elmimps = elmret.addElement("impuestos");

          axmlImpuestos = ATECOFFGenerarXmlData.methodSeleccionarImpuesReten(conn, crcDato.getId());

          String strNumDocSus = "";
          
          for (int i = 0; i < (3 - crcDato.getAtecfeNroEstab().length()); i++) {
              strNumDocSus = strNumDocSus + "0";
            }

            strNumDocSus = strNumDocSus + crcDato.getAtecfeNroEstab();

            for (int i = 0; i < (3 - crcDato.getAtecfePuntoEmision().length()); i++) {
              strNumDocSus = strNumDocSus + "0";
            }

            strNumDocSus = strNumDocSus + crcDato.getAtecfePuntoEmision();

            for (int i = 0; i < (9 - crcDato.getNoComprobanteVenta().length()); i++) {
              strNumDocSus = strNumDocSus + "0";
            }

            strNumDocSus = strNumDocSus + crcDato.getNoComprobanteVenta();

          /*for (int i = 0; i < (3 - invDatoRete.getCoNroEstab().length()); i++) {
            strNumDocSus = strNumDocSus + "0";
          }

          strNumDocSus = strNumDocSus + invDatoRete.getCoNroEstab();

          for (int i = 0; i < (3 - invDatoRete.getCoPuntoEmision().length()); i++) {
            strNumDocSus = strNumDocSus + "0";
          }

          strNumDocSus = strNumDocSus + invDatoRete.getCoPuntoEmision();

          for (int i = 0; i < (9 - invDatoRete.getDocumentNo().length()); i++) {
            strNumDocSus = strNumDocSus + "0";
          }

          strNumDocSus = strNumDocSus + invDatoRete.getDocumentNo();*/

          if (axmlImpuestos != null && axmlImpuestos.length > 0) {

            for (int i = 0; i < axmlImpuestos.length; i++) {
              Element elmtolimp = elmimps.addElement("impuesto");

              if (!axmlImpuestos[i].dato1.equals("")) {
                elmtolimp.addElement("codigo").addText(axmlImpuestos[i].dato1);
              } else {
                msg.setType("Error");
                msg.setMessage("El tipo de retención tiene que tener un código");
                msg.setTitle("@Error@");
                return false;
              }

              if (!axmlImpuestos[i].dato2.equals("")) {
                elmtolimp.addElement("codigoRetencion").addText(axmlImpuestos[i].dato2);
              } else {
                msg.setType("Error");
                msg.setMessage("La retención tiene que tener un código");
                msg.setTitle("@Error@");
                return false;
              }

              elmtolimp.addElement("baseImponible").addText(axmlImpuestos[i].dato3);
              elmtolimp.addElement("porcentajeRetener").addText(axmlImpuestos[i].dato4);
              elmtolimp.addElement("valorRetenido").addText(axmlImpuestos[i].dato5);
              elmtolimp.addElement("codDocSustento").addText(axmlImpuestos[i].dato6);
              elmtolimp.addElement("numDocSustento").addText(strNumDocSus);
              elmtolimp.addElement("fechaEmisionDocSustento").addText(axmlImpuestos[i].dato7);
            }

          } else {
            msg.setType("Error");
            msg.setMessage("La retención tiene que tener detalles");
            msg.setTitle("@Error@");
            return false;
          }

          //ATECOFF_Operacion_Auxiliares.addCamposAdic(elmret, conn, invDatoRete.getClient().getId());
          ATECOFF_Operacion_Auxiliares.addCamposAdic(elmret, conn, crcDato.getClient().getId(), crcDato.getOrganization().getId(), crcDato.getBpartner().getId(), crcDato.getId());
          
          final XMLWriter writer = new XMLWriter(
              new OutputStreamWriter(new FileOutputStream(flXml), "utf-8"), ofFormat);
          writer.write(docXML);
          writer.flush();
          writer.close();

          /*flXml = ATECOFF_Operacion_Auxiliares.firmarDocumento(flXml, conn, strUser,
              invDatoRete.getClient().getId(), msg);*/
          
          flXml = ATECOFF_Operacion_Auxiliares.firmarDocumento(flXml, conn, strUser,
        		  crcDato.getClient().getId(), msg);

          if (flXml != null) {
            if (enviarSRI) {
              Calendar cldFecha = new GregorianCalendar();
              String strFechaAut = "";

              strFechaAut = sdfFormato.format(cldFecha.getTime());

              byte[] bytes = ATECOFF_Operacion_Auxiliares.filetobyte(flXml);
              fileString = new String(bytes, "UTF-8");

              String mensaje = "";

              ATECOFF_SRIDocumentoAutorizado autorizadoPre = new ATECOFF_SRIDocumentoAutorizado();
              ATECOFF_SRIDocumentoRecibido recibido = new ATECOFF_SRIDocumentoRecibido();

              ATECOFF_ServiceAutorizacion serviceAutorizacionPre = new ATECOFF_ServiceAutorizacion(
                  cltDato.getAtecfeTipoambiente(), hstClaveAcceso.get("claveacc"));

              autorizadoPre = serviceAutorizacionPre.CallAutorizado();

              if (!autorizadoPre.getEstadoespecifico().equals("AUT")) {
                ATECOFF_ServiceRecibido serviceRecibido = new ATECOFF_ServiceRecibido(
                    cltDato.getAtecfeTipoambiente(), fileString);
                recibido = serviceRecibido.CallRecibido();
              } else {
                recibido.setEstado("RECIBIDO");
                recibido.setEstadoespecifico("REC");
              }

              if (recibido.getInformacion() != null) {
                mensaje = recibido.getMensaje() + " - "
                    + recibido.getInformacion().replace("'", "");
              }

              if (recibido.getEstadoespecifico().equals("REC")) {
                ATECOFF_SRIDocumentoAutorizado autorizado = new ATECOFF_SRIDocumentoAutorizado();
                ATECOFF_ServiceAutorizacion serviceAutorizacion = new ATECOFF_ServiceAutorizacion(
                    cltDato.getAtecfeTipoambiente(), hstClaveAcceso.get("claveacc"));
                autorizado = serviceAutorizacion.CallAutorizado();

                if (autorizado.getEstadoespecifico().equals("AUT")) {
                  if (autorizado.getInformacion() != null) {
                    mensaje = autorizado.getMensaje() + " - "
                        + autorizado.getInformacion().replace("'", "");
                  }

                  List<File> lisdoc = new ArrayList<File>();
                  lisdoc.add(autorizado.getDocFile());

                  File flPdf = null;

                  if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("7")
                      || dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("07")) {
                    flPdf = ATECOFF_Operacion_Auxiliares.generarPDF(conn,
                        "@basedesign@/com/atrums/felectronica/erpReport/Rpt_Retenciones.jrxml",
                        "Retencion", crcDato.getId());
                  }

                  if (flPdf != null)
                    lisdoc.add(flPdf);

                  /*axmlEmail = ATECOFFGenerarXmlData.methodSeleccionarEmail(conn,
                      invDatoRete.getBusinessPartner().getId());*/
                  
                  axmlEmail = ATECOFFGenerarXmlData.methodSeleccionarEmail(conn,
                		  crcDato.getBpartner().getId());

                  String strSubject = "Retención Electrónica de " + axmlEmail[0].dato2;
				  
                 String strContenido = "";

                  if (cltDato.isAtecoffIspostal()) {
                    strContenido = "Señor/a\n" + axmlEmail[0].dato3
                        + "\n\nUd tiene un documento electronico que puede ser consultada en: "
                        + axmlEmail[0].dato4 + "\nCon los credeciales: \n\n -Usuario: "
                        + axmlEmail[0].dato5 + "\n -Contraseña: " + axmlEmail[0].dato5
                        + "\n\n\nAtentamente " + axmlEmail[0].dato2;
                  } else {
                    /*strContenido = "Señor/a\n" + axmlEmail[0].dato3
                        + "\n\nUd tiene un documento electronico" + "\n\n\nAtentamente "
                        + axmlEmail[0].dato2;*/
                	  
                  	String nomDoc = "Retención de Compra Electrónica";
                  	type = "text/html; charset=utf-8";
                    strContenido = "<table style=\"width: 85%; padding: 10px; margin:0 auto; border-collapse: collapse;font-family: sans-serif\">\r\n" + 
                      		"	<tr style=\"background-color: #003764\">\r\n" + 
                      		"		<td style=\"width: 85%\">\r\n" + 
                      		"		    <a target=\"_blank\" href=\"http://atrums.com/\">\r\n" + 
                      		"			   <img width=\"20%\" style=\"display:block; margin: 1.5% 3%\" src=\"http://ws.atrums.com/portal/images/publicidad/atrumsit-logo.png\">\r\n" + 
                      		"			</a></td>\r\n" + 
                      		"		<td style=\"width: 3%\">\r\n" + 
                      		"			<a target=\"_blank\" href=\"https://www.facebook.com/Atrumsit-393603567398708/\">\r\n" + 
                      		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/facebook.png\"/>\r\n" + 
                      		"			</a></td>\r\n" + 
                      		"        <td style=\"width: 3%\">		\r\n" + 
                      		"			<a target=\"_blank\" href=\"https://www.instagram.com/atrumsit/?hl=es-la\">\r\n" + 
                      		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/instagram.png\"/>\r\n" + 
                      		"			</a></td>\r\n" + 
                      		"        <td style=\"width: 3%\">		\r\n" + 
                      		"			<a target=\"_blank\" href=\"https://www.youtube.com/channel/UCQ6Vl9DHMH3NNa93HxsqAUw?view_as=subscriber\">\r\n" + 
                      		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/youtube.png\"/>\r\n" + 
                      		"			</a></td>\r\n" + 
                      		"        <td style=\"width: 3%\">	\r\n" + 
                      		"			<a target=\"_blank\" href=\"https://twitter.com/atrumsit\">\r\n" + 
                      		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/twitter.png\"/>\r\n" + 
                      		"			</a></td>	\r\n" + 
                      		"		<td style=\"width: 3%\">\r\n" + 
                      		"			<a target=\"_blank\" href=\"https://au.linkedin.com/company/atrumsit\">\r\n" + 
                      		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/linkedin.png\"/>\r\n" + 
                      		"			</a></td>\r\n" + 
                      		"	</tr>\r\n" + 
                      		"	<tr>\r\n" + 
                      		"		<td style=\"background-color: #ECF0F1\" colspan=\"6\">\r\n" + 
                      		"		  <div style=\"border: 6px solid #FFFFFF\">\r\n" + 
                      		"			<div style=\"color: #34495e; margin: 2% 6% 2%; text-align: justify;font-size: 14px\">\r\n" + 
                      		"				<h2 style=\"color: #8DC63F\">Estimad@:</h2>\r\n" + 
                      		"				<p><b>"+ axmlEmail[0].dato3 +"</b></p>\r\n" + 
                      		"				<p>Usted tiene una "+ nomDoc +" generada por la empresa <b>"+ axmlEmail[0].dato2 +".</b></p>\r\n" + 
                      		"				<div style=\"width: 100%;text-align: center;margin-top: 38px\">\r\n" + 
                      		"					Si quiere conocer más de nuestros servicios, haga clic aquí<br><br>\r\n" + 
                      		"					<a style=\"text-decoration: none; border-radius: 5px; padding: 8px 18px; color: white; background-color: #8DC63F\" target=\"_blank\" href=\"http://atrums.com/\">ATRUMS IT</a>	\r\n" + 
                      		"				</div>	\r\n" + 
                      		"				<div style=\"font-size: 12px\">\r\n" + 
                      		"					<p style=\"margin-top:50px\">\r\n" + 
                      		"					   La información y archivos adjuntos contenidos en este mensaje electrónico son confidenciales y reservados; por tanto no pueden ser usados, reproducidos o divulgados por otras personas distintas a su(s) destinatario(s). \r\n" + 
                      		"					   Si Ud. no es el destinatario de este email, le solicitamos comedidamente eliminarlo.\r\n" + 
                      		"					</p>\r\n" + 
                      		"					<p>\r\n" + 
                      		"					   Recuerde que el documento electrónico cumple con todas las disposiciones establecidas en el marco legal vigente y sustituye al documento en formato impreso con igual valor legal.<br> \r\n" + 
                      		"					   Le recomendamos no imprimir este correo electrónico a menos que sea estrictamente necesario.\r\n" + 
                      		"					</p>\r\n" + 
                      		"					<p>\r\n" + 
                      		"					   Por favor, no responda a este correo electrónico.\r\n" + 
                      		"					<p>\r\n" + 
                      		"				</div>\r\n" + 
                      		"			</div>\r\n" + 
                      		"		  </div>\r\n" + 
                      		"		</td>\r\n" + 
                      		"	</tr>\r\n" + 
                      		"	<tr>\r\n" + 
                      		"	  <td colspan=\"6\">\r\n" + 
                      		"		<table style=\"width: 100%\">\r\n" + 
                      		"		    <tr style=\"color: #FFFFFF;background-color: #003764;height: 40px\">\r\n" + 
                      		"				<td style = \"width: 100%;font-size: 13px\">\r\n" + 
                      		"				    <div style=\"width:98%;margin: 1% 2% 1%\">\r\n" + 
                      		"						<div><img style=\"width: 14px;\" src=\"http://ws.atrums.com/portal/images/publicidad/ubicacion.png\"/>\r\n" + 
                      		"							 <a target=\"_blank\" href=\"http://atrums.com/\" style=\"color: #FFFFFF\">www.atrums.com</a>\r\n" + 
                      		"						</div>\r\n" + 
                      		"						<div><img style=\"width: 14px;\" src=\"http://ws.atrums.com/portal/images/publicidad/email.png\"/>\r\n" + 
                      		"							 <a style=\"text-decoration: none;color: #FFFFFF\">info@atrums.com</a>\r\n" + 
                      		"						</div>\r\n" + 
                      		"						<div style=\"font-size: 12px\">\r\n" + 
                      		"							 <img style=\"width: 10px;\" src=\"http://ws.atrums.com/portal/images/publicidad/telefono.png\"/>\r\n" + 
                      		"							 022247848 | 022241461 | <a target=\"_blank\" href=\"https://web.whatsapp.com/\" style=\"color: #FFFFFF\">0958743792</a>\r\n" + 
                      		"						</div>\r\n" + 
                      		"					</div>\r\n" + 
                      		"				</td>\r\n" + 
                      		"			</tr>\r\n" + 
                      		"		</table>\r\n" + 
                      		"	  </td>\r\n" + 
                      		"	</tr>\r\n" + 
                      		"</table>";                	  
                  }
				  
                  String strMensaje = "Su documento a sido autorizado por el SRI, ";

                  if (ATECOFF_Operacion_Auxiliares.enviarCorreo(axmlEmail[0].dato1, strSubject,
                      strContenido, type, lisdoc, false)) {
                    msg.setType("Success");
                    msg.setTitle("Mensaje");
                    msg.setMessage(strMensaje + "y fue enviado al correo electronico del cliente");
                    flXml.delete();

                    mensaje = mensaje.equals("") ? "Se envio el email cliente, AUTORIZADO"
                        : ", Se envio el email al cliente, AUTORIZADO";

                    ATECOFFGenerarXmlData.methodActualizarRetenOffline(conn, autorizado.getDocXML(),
                        hstClaveAcceso.get("claveacc"), mensaje, "PD", "AP", "PD", "AP",
                        hstClaveAcceso.get("claveacc"), strFechaAut, crcDato.getId());
                    return true;
                  } else {
                    mensaje = mensaje.equals("") ? "No se envio el email cliente, AUTORIZADO"
                        : ", No se envio el email al cliente, AUTORIZADO";

                    ATECOFFGenerarXmlData.methodActualizarRetenOffline(conn, autorizado.getDocXML(),
                        hstClaveAcceso.get("claveacc"), mensaje, "PD", "AP", "PD", "AP",
                        hstClaveAcceso.get("claveacc"), strFechaAut, crcDato.getId());
                    return true;
                  }
                } else {
                  if (autorizado.getEstadoespecifico().equals("N")) {
                    ATECOFFGenerarXmlData.methodActualizarRetEstadoOffline(conn, "PD",
                        crcDato.getId());

                    msg.setType("Error");
                    msg.setMessage(mensaje);
                    msg.setTitle("@Error@");
                    return false;
                  }

                  if (autorizado.getInformacion() != null) {
                    mensaje = autorizado.getMensaje() + " - "
                        + autorizado.getInformacion().replace("'", "");
                  }

                  String strSubject = null;

                  if (cltDato.getAtecfeTipoambiente().equals("1")) {
                    strSubject = "Documento Electrónico Rechazada Ambiente Pruebas";
                  } else {
                    strSubject = "Documento Electrónico Rechazada Ambiente Producción";
                  }

                  String strContenido = "Estimado hay un error en el documento retencion de la empresa "
                      + cltDato.getName() + ": " + crcDato.getDocumentNo() + " con clave de acceso "
                      + hstClaveAcceso.get("claveacc") + "\nEl error es: " + mensaje;

                  /*if (ATECOFF_Operacion_Auxiliares.enviarCorreo(emailSoporte, strSubject,
                      strContenido, null, null, true)) {
                  */
                  if (true) {

                    ATECOFFGenerarXmlData.methodActualizarRetenOffline(conn, autorizado.getDocXML(),
                        hstClaveAcceso.get("claveacc"), mensaje, "PD", "RZ", "PD", "RZ",
                        hstClaveAcceso.get("claveacc"), strFechaAut, crcDato.getId());

                    msg.setType("Error");
                    msg.setMessage(mensaje);
                    msg.setTitle("@Error@");
                    return false;
                  } else {
                    ATECOFFGenerarXmlData.methodActualizarRetenOffline(conn, autorizado.getDocXML(),
                        hstClaveAcceso.get("claveacc"), mensaje, "PD", "PD", "PD", "PD",
                        hstClaveAcceso.get("claveacc"), strFechaAut, crcDato.getId());
                    return false;
                  }
                }
              } else {

                String strSubject = null;

                if (cltDato.getAtecfeTipoambiente().equals("1")) {
                  strSubject = "Documento Electrónico Rechazada Ambiente Pruebas";
                } else {
                  strSubject = "Documento Electrónico Rechazada Ambiente Producción";
                }

                String strContenido = "Estimado hay un error en el documento retencion de la empresa "
                    + cltDato.getName() + ": " + crcDato.getDocumentNo() + " con clave de acceso "
                    + hstClaveAcceso.get("claveacc") + "\nEl error es: " + mensaje;

                /*if (ATECOFF_Operacion_Auxiliares.enviarCorreo(emailSoporte, strSubject,
                    strContenido, null, null, true)) {
                */
                
                if ( true) {
                  ATECOFFGenerarXmlData.methodActualizarRetenOffline(conn, null,
                      hstClaveAcceso.get("claveacc"), mensaje, "PD", "RZ", "PD", "RZ",
                      hstClaveAcceso.get("claveacc"), strFechaAut, crcDato.getId());

                  msg.setType("Error");
                  msg.setMessage(mensaje);
                  msg.setTitle("@Error@");
                  return false;
                } else {
                  ATECOFFGenerarXmlData.methodActualizarRetenOffline(conn, null,
                      hstClaveAcceso.get("claveacc"), mensaje, "PD", "PD", "PD", "PD",
                      hstClaveAcceso.get("claveacc"), strFechaAut, crcDato.getId());
                  return false;
                }
              }
            } else {
              Calendar cldFecha = new GregorianCalendar();
              String strFechaAut = "";

              strFechaAut = sdfFormato.format(cldFecha.getTime());

              ATECOFFGenerarXmlData.methodActualizarRetenOffline(conn, null,
                  hstClaveAcceso.get("claveacc"), null, "PD", "PD", "PD", "PD",
                  hstClaveAcceso.get("claveacc"), strFechaAut, crcDato.getId());

              msg.setType("Success");
              msg.setTitle("Mensaje");
              msg.setMessage("Documento Procesado Electrónicamente");
              flXml.delete();

              return true;
            }

          } else {
            return false;
          }

        } else {
          return false;
        }
      }

      msg.setType("Error");
      msg.setMessage("No hay un tercero en la retención");
      msg.setTitle("@Error@");
      return false;
    } finally {
      flXml.delete();
      flXml = null;

      fileString = null;

      docXML.clearContent();
      docXML = null;
      ofFormat = null;

      dctDato = null;
      cltDato = null;
      oriDato = null;
      bspDato = null;
      /*invDatoRete = null;*/
      oriDatoAux = null;

      axmlDirMatriz = null;
      axmlDirec = null;
      axmlImpuestos = null;
      axmlEmail = null;

      hstClaveAcceso.clear();
    }
  }
}
