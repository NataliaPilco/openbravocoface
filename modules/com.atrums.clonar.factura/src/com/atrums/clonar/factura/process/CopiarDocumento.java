/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2010 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.atrums.clonar.factura.process;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.exception.OBException;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.service.json.DataResolvingMode;
import org.openbravo.xmlEngine.XmlDocument;

import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Query;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.pricing.pricelist.PriceListVersion;
import org.openbravo.service.db.CallStoredProcedure;
import org.openbravo.service.json.DataToJsonConverter;

import org.jfree.util.Log;
import org.apache.log4j.Logger;
import org.openbravo.dal.core.SessionHandler;

public class CopiarDocumento extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  @Override
  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    final VariablesSecureApp vars = new VariablesSecureApp(request);

    OBContext.setAdminMode(true);
 
    if (vars.commandIn("DEFAULT")) {
        String strProcessId = vars.getStringParameter("inpProcessId");
        String strWindow = vars.getStringParameter("inpwindowId");
        String strTab = vars.getStringParameter("inpTabId");
        String strKey = vars.getGlobalVariable("inpcInvoiceId", strWindow + "|C_Invoice_ID");
        printPage(response, vars, strKey, strWindow, strTab, strProcessId);
      } else if (vars.commandIn("BUSCARTIPO")) {
        String strWindow = vars.getStringParameter("inpWindowId");
        String strTab = vars.getStringParameter("inpTabId");
        String strDocumentType = vars.getStringParameter("inpTipoDoc");
        String strCInvoiceId = vars.getStringParameter("inpcInvoiceId");
        
        printPageTipo(response, vars, strCInvoiceId, strWindow, strTab, vars.getClient(),
        		strDocumentType);
        
      } else if (vars.commandIn("SAVE")) {            
         	 
	        String strTab = vars.getStringParameter("inpTabId");
	        String strCInvoiceId = vars.getStringParameter("inpcInvoiceId");
	        String strDocumentType = vars.getStringParameter("inpTipoDoc");

	        String strWindowPath = Utility.getTabURL(strTab, "R", true);
	        if (strWindowPath.equals("")) {
	          strWindowPath = strDefaultServlet;
	        }
	        
		    OBError myError = processButton(vars, strCInvoiceId, strDocumentType);
		    log4j.debug(myError.getMessage());
		    vars.setMessage(strTab, myError);
		    printPageClosePopUp(response, vars, strWindowPath);            
     }   
  }
  
  private void printPageTipo(HttpServletResponse response, VariablesSecureApp vars,
	      String strCInvoiceId, String windowId, String strTab, String strClient,
	      String strDocumentType) throws IOException, ServletException {
	  
	    try {

	      ComboTableData comboTableDataTipoDoc = new ComboTableData(vars, this, "TABLEDIR",
	          "CF_TIPO_DOCUMENTO_ID", "", null, Utility.getReferenceableOrg(vars, vars.getOrg()),
	          Utility.getContext(this, vars, "#User_Client", windowId), 0);

	      XmlDocument xmlDocument = xmlEngine
	          .readXmlTemplate("com/atrums/clonar/factura/process/CopiarDocumento")
	          .createXmlDocument();
	      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
	      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
	      xmlDocument.setParameter("theme", vars.getTheme());
	      xmlDocument.setParameter("window", windowId);
	      xmlDocument.setParameter("tab", strTab);
	      xmlDocument.setParameter("cinvoiceid", strCInvoiceId);

	      Utility.fillSQLParameters(this, vars, null, comboTableDataTipoDoc, windowId, null);
	      xmlDocument.setData("reportTipoDoc_ID", "liststructure",
	    		  comboTableDataTipoDoc.select(false));

	      xmlDocument.setParameter("tipodocid", strDocumentType);

	      response.setContentType("text/html; charset=UTF-8");
	      PrintWriter out = response.getWriter();
	      out.println(xmlDocument.print());
	      out.close();

	    } catch (Exception ex) {
	      // TODO: handle exception
	      throw new ServletException(ex);
	    }
	  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strKeyValue, String windowId, String strTab, String strProcessId)
      throws IOException, ServletException {

    try {   
        ComboTableData comboTableDataTipoDoc = new ComboTableData(vars, this, "TABLEDIR",
            "CF_TIPO_DOCUMENTO_ID", "", null, Utility.getReferenceableOrg(vars, vars.getOrg()),
            Utility.getContext(this, vars, "#User_Client", windowId), 0);

        XmlDocument xmlDocument = xmlEngine
            .readXmlTemplate("com/atrums/clonar/factura/process/CopiarDocumento")
            .createXmlDocument();
        xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
        xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
        xmlDocument.setParameter("theme", vars.getTheme());
        xmlDocument.setParameter("window", windowId);
        xmlDocument.setParameter("tab", strTab);
        xmlDocument.setParameter("cinvoiceid", strKeyValue);

        Utility.fillSQLParameters(this, vars, null, comboTableDataTipoDoc, windowId, null);
        xmlDocument.setData("reportTipoDoc_ID", "liststructure",
        		comboTableDataTipoDoc.select(false));

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println(xmlDocument.print());
        out.close();

      } catch (Exception ex) {
        // TODO: handle exception
        throw new ServletException(ex);
      }
  }
  
  private OBError processButton(VariablesSecureApp vars, String strCInvoiceId, String strDocumentType) {
	    OBError myError = null;

	    OBContext.setAdminMode(true);
	    try {
	    	
	      CopiarDocumentoData[] enviarData = null;
	      enviarData = CopiarDocumentoData.methodCrearDocumento(this, strCInvoiceId, strDocumentType);
 	       
	      myError = new OBError();
	      myError.setType("Success");
	      myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
	      myError.setMessage(Utility.messageBD(this, "Documento creado: " + enviarData[0].dato1, vars.getLanguage()));
	    
	    } catch (Exception e) {
	      OBDal.getInstance().rollbackAndClose();

	      e.printStackTrace();
	      log4j.warn("Rollback in transaction");
	      myError = new OBError();
	      myError.setType("Error");
	      myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
	      myError.setMessage("El proceso no se ejecuto correctamente: " + e.getMessage());
	    } finally {
	      OBContext.restorePreviousMode();
	    }

	    return myError;
	  }  

  @Override
  public String getServletInfo() {
    return "Servlet that presents the business partners seeker";
  } // end of getServletInfo() method
}
