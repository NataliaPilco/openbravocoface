<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="DeclaracionIVAVentas" pageWidth="935" pageHeight="842" whenNoDataType="AllSectionsNoDetail" columnWidth="935" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="e5e9e7fd-4872-41a3-adf3-da7e218bf1e9">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="style1">
		<conditionalStyle>
			<conditionExpression><![CDATA[$F{tipo_documento}.equals("APC")]]></conditionExpression>
			<style forecolor="#FF0000"/>
		</conditionalStyle>
	</style>
	<parameter name="USER_CLIENT" class="java.lang.String"/>
	<parameter name="DateFrom" class="java.util.Date"/>
	<parameter name="DateTo" class="java.util.Date"/>
	<queryString>
		<![CDATA[select c_doctypetarget_id, name, sum(num_fact) as num_fact,
       sum(case when tipo_documento='APC' then subtotal_exento*(-1) else subtotal_exento end) as subtotal_exento,
       sum(case when tipo_documento='APC' then subtotal_iva0*(-1) else subtotal_iva0 end) as subtotal_iva0,
       sum(case when tipo_documento='APC' then subtotal_iva*(-1) else subtotal_iva end) as subtotal_iva,
       sum(case when tipo_documento='APC' then iva*(-1) else iva end) as iva,
       sum(case when tipo_documento='APC' then ice*(-1) else ice end) as ice,
       sum(case when tipo_documento='APC' then servicio*(-1) else servicio end) as servicio,
       sum(case when tipo_documento='APC' then otro*(-1) else otro end) as otro,
       sum(case when tipo_documento='APC' then total*(-1) else total end) as total,
       sum(case when tipo_documento='APC' then num_ret*(-1) else num_ret end) as num_ret,
       sum(case when tipo_documento='APC' then num_ret_iva*(-1) else num_ret_iva end) as num_ret_iva,
       sum(case when tipo_documento='APC' then valor_ret*(-1) else valor_ret end) as valor_ret,
       sum(case when tipo_documento='APC' then ret_base*(-1) else ret_base end) as ret_base,
       tipo_documento
  from(
select c_invoice_id, c_doctypetarget_id, name, tipo_documento, 1 as num_fact, sum(subtotal_exento) as subtotal_exento,
       sum(subtotal_iva0) as subtotal_iva0, sum(subtotal_iva) as subtotal_iva, sum(iva) as iva,
       sum(ice) as ice, sum(servicio) as servicio, sum(otro) as otro, total,
       (case when num_ret = 1 then 1 else 0 end) as num_ret,
       num_ret_iva as num_ret_iva, sum(ret_base) as ret_base, sum(valor_ret) as valor_ret
  from(
select c_invoice_id, c_doctypetarget_id, name, tipo_documento, num_fact,
       sum(subtotal_exento) as subtotal_exento, sum(subtotal_iva0) as subtotal_iva0,
       sum(subtotal_iva) as subtotal_iva, sum(iva) as iva, sum(ice) as ice,
       sum(servicio) as servicio, sum(otro) as otro,
       total, num_ret, num_ret_iva, ret_base, valor_ret
  from(
select  distinct(i.c_invoice_id) as c_invoice_id,i.c_doctypetarget_id, d.name,coalesce(docbasetype,' ')as tipo_documento, i.documentno as num_fact,
        (case when tc.em_co_tp_base_imponible = '1' then it.taxbaseamt else 0 end) as subtotal_exento, -- exento
        (case when tc.em_co_tp_base_imponible = '2' then it.taxbaseamt else 0 end) as subtotal_iva0, -- iva 0%
        (case when tc.em_co_tp_base_imponible = '3' then it.taxbaseamt else 0 end) as subtotal_iva, -- -- 12% o 14%
        (case when  upper(t.name) like upper('%iva%') then it.taxamt else 0 end) as iva,
        (case when  upper(t.name) like upper('%ice%') then it.taxamt else 0 end) as ice,
        (case when  upper(t.name) like upper('%servicio%') then it.taxamt else 0 end) as servicio,
        (case when (upper(t.name) not like upper('%servicio%') and
                             upper(t.name) not like upper('%iva%') and
                             upper(t.name) not like upper('%ice%')) then it.taxamt else 0 end) as otro,
        coalesce(i.grandtotal,0) as total,
        (case when rv.documentno is not null then 1 else 0 end) as num_ret,
        (case when rvl.tipo='IVA' then 1 else 0 end) as num_ret_iva,
        (case when rvl.tipo='IVA' then sum(rvl.base_imp_retencion) else 0 end)  as ret_base,
        (case when rvl.tipo='IVA' then sum(rvl.valor_retencion) else 0 end) as valor_ret
     from c_invoice i
     left join c_doctype d on (i.c_doctypetarget_id=d.c_doctype_id)
     left join c_invoicetax it on (i.c_invoice_id=it.c_invoice_id)
     left join c_tax t on (it.c_tax_id = t.c_tax_id)
     left join c_taxcategory tc on (t.c_taxcategory_id = tc.c_taxcategory_id)
     left join co_retencion_compra rv on (rv.c_invoice_id=i.c_invoice_id and
                                         rv.docstatus='CO' and
                                         date(rv.fecha_emision) >= date($P{DateFrom}) and
                                         date(rv.fecha_emision) <= date($P{DateTo}))
     left join co_retencion_compra_linea rvl on (rv.co_retencion_compra_id=rvl.co_retencion_compra_id and rvl.tipo='IVA')
    where date(i.dateinvoiced)>= date($P{DateFrom})
      and date(i.dateinvoiced)<= date($P{DateTo})
      and i.issotrx='N'
      and i.docstatus = 'CO'
      and i.ad_client_id in ($P!{USER_CLIENT})
    group by i.c_invoice_id,rvl.tipo, d.name, d.docbasetype, tc.em_co_tp_base_imponible, t.name, i.grandtotal, it.taxbaseamt,
          it.taxamt, rv.documentno
    order by 1
    ) as reportes
    group by 1,2,3,4,5,13,14,15,16,17
    ) as datos
    group by 1,2,3,4,13,14,15
) as datos
group by 1,2,16
order by 1]]>
	</queryString>
	<field name="c_doctypetarget_id" class="java.lang.String"/>
	<field name="name" class="java.lang.String"/>
	<field name="num_fact" class="java.lang.Long"/>
	<field name="subtotal_exento" class="java.math.BigDecimal"/>
	<field name="subtotal_iva0" class="java.math.BigDecimal"/>
	<field name="subtotal_iva" class="java.math.BigDecimal"/>
	<field name="iva" class="java.math.BigDecimal"/>
	<field name="ice" class="java.math.BigDecimal"/>
	<field name="servicio" class="java.math.BigDecimal"/>
	<field name="otro" class="java.math.BigDecimal"/>
	<field name="total" class="java.math.BigDecimal"/>
	<field name="num_ret" class="java.lang.Long"/>
	<field name="num_ret_iva" class="java.lang.Long"/>
	<field name="valor_ret" class="java.math.BigDecimal"/>
	<field name="ret_base" class="java.math.BigDecimal"/>
	<field name="tipo_documento" class="java.lang.String"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<detail>
		<band height="12" splitType="Stretch">
			<textField>
				<reportElement x="0" y="0" width="155" height="12" uuid="bd76d917-1296-4971-a1c0-b941526159b5"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{name}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="155" y="0" width="60" height="12" uuid="2ed6cbf7-825c-4180-b958-c5e8c5494e2e"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{num_fact}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="215" y="0" width="60" height="12" uuid="0dd21d00-72d1-4f27-997b-3c14d8e72f75"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{subtotal_exento}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="395" y="0" width="60" height="12" uuid="9df43fd7-c67b-4077-8810-e9a7a8e8dcec"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{iva}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="455" y="0" width="60" height="12" uuid="845b757c-56a3-4799-9e10-c94bdf80f605"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{servicio}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="635" y="0" width="60" height="12" uuid="8583ccbb-c6cb-4011-815a-951b1423bb5a"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{total}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="515" y="0" width="60" height="12" uuid="fa7e91ab-d733-4996-8639-aea0e0122375"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ice}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="575" y="0" width="60" height="12" uuid="8757dece-a770-4228-9c8d-ed1586d11e52"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{otro}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="695" y="0" width="60" height="12" uuid="e1b6471f-a61d-4170-ae01-58de0b66b0ea"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{num_ret}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="815" y="0" width="60" height="12" uuid="b5e84834-f560-435a-842b-b995aca1e68e"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ret_base}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="875" y="0" width="60" height="12" uuid="32c20943-f09e-4798-9cc9-69dc9b7d6b3f"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{valor_ret}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="275" y="0" width="60" height="12" uuid="af6e3956-54b1-4fdc-9296-6dabf5342caf"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{subtotal_iva0}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="335" y="0" width="60" height="12" uuid="1d6867d4-a3a3-4f32-aad2-e51e5458568d"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{subtotal_iva}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="755" y="0" width="60" height="12" uuid="73448896-4fe4-446d-a950-32c5dd777771"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{num_ret_iva}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
