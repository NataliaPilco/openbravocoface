<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="DeclaracionIVAVentas" pageWidth="935" pageHeight="842" whenNoDataType="AllSectionsNoDetail" columnWidth="935" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="18bacdf0-30f9-4526-ad7d-77a84448b493">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="USER_CLIENT" class="java.lang.String"/>
	<parameter name="DateFrom" class="java.util.Date"/>
	<parameter name="DateTo" class="java.util.Date"/>
	<queryString>
		<![CDATA[select 'total' as name,
       sum(num_fact) as num_fact,
       sum(subtotal_exento) as subtotal_exento,
       sum(subtotal_iva0) as subtotal_iva0,
       sum(subtotal_iva) as subtotal_iva,
       sum(iva) as iva,
       sum(ice) as ice,
       sum(servicio) as servicio,
       sum(otro) as otro,
       sum(total) as total,
       sum(num_ret) as num_ret,
       sum(num_ret_iva) as num_ret_iva,
       sum(valor_ret) as valor_ret,
       sum(ret_base) as ret_base
from(
select sum(num_fact) as num_fact,
       sum(case when tipo_documento='ARC' then subtotal_exento*(-1) else subtotal_exento end) as subtotal_exento,
       sum(case when tipo_documento='ARC' then subtotal_iva0*(-1) else subtotal_iva0 end) as subtotal_iva0,
       sum(case when tipo_documento='ARC' then subtotal_iva*(-1) else subtotal_iva end) as subtotal_iva,
       sum(case when tipo_documento='ARC' then iva*(-1) else iva end) as iva,
       sum(case when tipo_documento='ARC' then ice*(-1) else ice end) as ice,
       sum(case when tipo_documento='ARC' then servicio*(-1) else servicio end) as servicio,
       sum(case when tipo_documento='ARC' then otro*(-1) else otro end) as otro,
       sum(case when tipo_documento='ARC' then total*(-1) else total end) as total,
       (case when tipo_documento='ARC' then num_ret*(-1) else (select count(distinct(rv.documentno))
                                                                  from c_invoice x, co_retencion_venta rv, co_retencion_venta_linea rvl
                                                                 where x.c_invoice_id = rv.c_invoice_id
                                                                   and rv.docstatus='CO'
                                                                   and date(rv.fecha_emision) >= date($P{DateFrom})
                                                                   and date(rv.fecha_emision) <= date($P{DateTo})
                                                                   and date(x.dateinvoiced) >= date($P{DateFrom})
                                                                   and date(x.dateinvoiced) <= date($P{DateTo})
                                                                   and rv.co_retencion_venta_id=rvl.co_retencion_venta_id
                                                                   and x.ad_client_id in ($P!{USER_CLIENT})) end) as num_ret,
       sum(case when tipo_documento='ARC' then num_ret_iva*(-1) else num_ret_iva end) as num_ret_iva,
       sum(case when tipo_documento='ARC' then valor_ret*(-1) else valor_ret end) as valor_ret,
       sum(case when tipo_documento='ARC' then ret_base*(-1) else ret_base end) as ret_base
  from(
select  c_doctypetarget_id, name, coalesce(docbasetype,' ') as tipo_documento, count(*) as num_fact,
        coalesce((case when em_co_tp_base_imponible = '1' then sum(COALESCE(taxbaseamt,0)) end),0) as subtotal_exento, -- exento
        coalesce((case when em_co_tp_base_imponible = '2' then sum(COALESCE(taxbaseamt,0)) end),0) as subtotal_iva0, -- iva 0%
        coalesce((case when em_co_tp_base_imponible = '3' then sum(COALESCE(taxbaseamt,0)) end),0) as subtotal_iva, -- -- 12% o 14%
        coalesce((case when  upper(impuesto) like upper('%iva%') then sum(taxamt) end),0) as iva,
        coalesce((case when  upper(impuesto) like upper('%ice%') then sum(taxamt) end),0) as ice,
        coalesce((case when  upper(impuesto) like upper('%servicio%') then sum(taxamt) end),0) as servicio,
        coalesce((case when (upper(impuesto) not like upper('%servicio%') and
                             upper(impuesto) not like upper('%iva%') and
                             upper(impuesto) not like upper('%ice%')) then sum(taxamt) end),0) as otro,
        coalesce(sum(grandtotal),0) as total,
        0  as num_ret,
        coalesce((case when tipo='IVA' then count(tipo) end),0)  as num_ret_iva,
        coalesce((case when tipo='IVA' then sum(base_imp_retencion) end),0)  as ret_base,
        coalesce((case when tipo='IVA' then sum(valor_retencion) end) ,0) as valor_ret
from(

select  i.c_doctypetarget_id,
        d.name,
        docbasetype,
        tc.em_co_tp_base_imponible,
        it.taxbaseamt,
        it.taxamt,
        t.name as impuesto,
        i.grandtotal,

        (select rvl.tipo
                           from co_retencion_venta rv, co_retencion_venta_linea rvl
                          where rv.c_invoice_id=i.c_invoice_id
                            and rv.docstatus='CO'
                            and date(rv.fecha_emision) >= date($P{DateFrom})
                            and date(rv.fecha_emision) <= date($P{DateTo})
                            and rv.co_retencion_venta_id=rvl.co_retencion_venta_id
                            and rvl.tipo='IVA') as tipo,
        (select sum(rvl.base_imp_retencion)
                   from co_retencion_venta rv, co_retencion_venta_linea rvl
                  where rv.c_invoice_id=i.c_invoice_id
                    and rv.docstatus='CO'
                    and date(rv.fecha_emision) >= date($P{DateFrom})
                    and date(rv.fecha_emision) <= date($P{DateTo})
                    and rv.co_retencion_venta_id=rvl.co_retencion_venta_id
                    and rvl.tipo='IVA') as base_imp_retencion,
        (select sum(rvl.valor_retencion)
           from co_retencion_venta rv, co_retencion_venta_linea rvl
          where rv.c_invoice_id=i.c_invoice_id
            and rv.docstatus='CO'
            and date(rv.fecha_emision) >= date($P{DateFrom})
            and date(rv.fecha_emision) <= date($P{DateTo})
            and rv.co_retencion_venta_id=rvl.co_retencion_venta_id
            and rvl.tipo='IVA') as valor_retencion
     from c_invoice i
     left join c_doctype d on (i.c_doctypetarget_id=d.c_doctype_id)
     left join c_invoicetax it on (i.c_invoice_id=it.c_invoice_id)
     left join c_tax t on (it.c_tax_id = t.c_tax_id)
     left join c_taxcategory tc on (t.c_taxcategory_id = tc.c_taxcategory_id)
    where date(i.dateinvoiced)>= date($P{DateFrom})
      and date(i.dateinvoiced)<= date($P{DateTo})
      and i.issotrx='Y'
      and i.docstatus = 'CO'
      and i.ad_client_id in ($P!{USER_CLIENT})
) as datos
group by 1,2,3,em_co_tp_base_imponible, impuesto, tipo, num_ret
order by 1
) as datos_ret
  group by 10
  order by 1
) as totales
group by 1]]>
	</queryString>
	<field name="name" class="java.lang.String"/>
	<field name="num_fact" class="java.math.BigDecimal"/>
	<field name="subtotal_exento" class="java.math.BigDecimal"/>
	<field name="subtotal_iva0" class="java.math.BigDecimal"/>
	<field name="subtotal_iva" class="java.math.BigDecimal"/>
	<field name="iva" class="java.math.BigDecimal"/>
	<field name="ice" class="java.math.BigDecimal"/>
	<field name="servicio" class="java.math.BigDecimal"/>
	<field name="otro" class="java.math.BigDecimal"/>
	<field name="total" class="java.math.BigDecimal"/>
	<field name="num_ret" class="java.math.BigDecimal"/>
	<field name="num_ret_iva" class="java.math.BigDecimal"/>
	<field name="valor_ret" class="java.math.BigDecimal"/>
	<field name="ret_base" class="java.math.BigDecimal"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<detail>
		<band height="14" splitType="Stretch">
			<textField>
				<reportElement x="155" y="0" width="60" height="14" uuid="6be375e4-b32a-4f22-83bc-13054c6d7c3d"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{num_fact}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="215" y="0" width="60" height="14" uuid="c93cbb38-4f2d-4648-8434-baed3352e641"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{subtotal_exento}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="395" y="0" width="60" height="14" uuid="5de929c4-b82f-4a6b-b59d-7a5a74ebd216"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{iva}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="455" y="0" width="60" height="14" uuid="6f69057d-191e-423f-bf80-1ef31cea12cf"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{servicio}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="635" y="0" width="60" height="14" uuid="d5f9d7dd-759e-40e9-99d5-d08e3a05dd2c"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{total}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="515" y="0" width="60" height="14" uuid="a21bc755-3adc-4ce5-b391-e1579abe458a"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ice}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="575" y="0" width="60" height="14" uuid="5b81a263-030b-41ea-a6d9-c117c21b0a18"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{otro}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="695" y="0" width="60" height="14" uuid="89261c4a-f910-4e4f-958b-34683552a576"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{num_ret}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="815" y="0" width="60" height="14" uuid="f9430cb1-d080-4b15-9b37-530fa27ed38d"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ret_base}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="875" y="0" width="60" height="14" uuid="1d2b376a-cdf6-42ac-9df4-a810e1f5e8e9"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{valor_ret}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="275" y="0" width="60" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="913cff9a-0497-46fb-99d7-8b8b6023d2e5"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{subtotal_iva0}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="335" y="0" width="60" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="944d15a4-eec4-427d-8fb6-16cf62a06f85"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{subtotal_iva}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="755" y="0" width="60" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="157425c2-d44f-4d35-abf1-bce353863fe4"/>
				<box>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{num_ret_iva}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Transparent" x="0" y="0" width="155" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="8ad43f7a-b7d8-40da-a5a5-b5081e6c1cf0"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" markup="none">
					<font fontName="SansSerif" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[TOTAL : ]]></text>
			</staticText>
		</band>
	</detail>
</jasperReport>
