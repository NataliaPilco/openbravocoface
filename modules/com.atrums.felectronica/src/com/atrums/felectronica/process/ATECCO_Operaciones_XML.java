package com.atrums.felectronica.process;

import org.apache.log4j.Logger;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.common.invoice.Invoice;

import com.atrums.offline.feletronica.process.ATECOFF_GenerarFacturaXML;

public class ATECCO_Operaciones_XML {
  static final Logger log = Logger.getLogger(ATECCO_Operaciones_XML.class);

  public ATECCO_Operaciones_XML() {
    super();
  }

  public boolean declararFacturaSRI(String strCinvoiceId, String varUser, OBError msg,
      ConnectionProvider connectionProvider) {
    try {

      // ATECFE_GenerarFactura facturar = new ATECFE_GenerarFactura();

      Invoice invDato = OBDal.getInstance().get(Invoice.class, strCinvoiceId);

      /*
       * if (!facturar.generarFacturaXML(strCinvoiceId, connectionProvider, varUser, msg)) { return
       * false; }
       */

      if (!ATECOFF_GenerarFacturaXML.generarFacturaXML(invDato, connectionProvider, varUser, false,
          msg)) {
        return false;
      }

      return true;
    } catch (Exception ex) {
      // TODO Auto-generated catch block
      log.warn(ex.getMessage());
      msg.setType("Error");
      msg.setMessage(ex.getMessage());
      msg.setTitle("@Error@");
      return false;
    }
  }
}
