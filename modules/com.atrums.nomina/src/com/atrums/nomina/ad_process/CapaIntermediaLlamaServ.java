package com.atrums.nomina.ad_process;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;

public class CapaIntermediaLlamaServ extends Thread {
  private static final Logger log = Logger.getLogger(CapaIntermediaServicio.class);
  private JSONArray servicioIds = null;

  public CapaIntermediaLlamaServ(JSONArray servicioIds) {
    this.servicioIds = servicioIds;
  }

  public void run() {
    try {
      if (this.servicioIds.length() > 0) {
        for (int i = 0; i < this.servicioIds.length(); i++) {
          final String servicioId = this.servicioIds.getString(i);
          CapaIntermediaServicio capaIntermediaSer = new CapaIntermediaServicio();
          capaIntermediaSer.setIdServicioLinea(servicioId);
          capaIntermediaSer.start();
          capaIntermediaSer.join();
        }
      }
    } catch (Exception e) {
      log.error(e.getMessage());
    }
  }
}
