package com.atrums.nomina.ad_process;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.openbravo.dal.core.SessionHandler;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.EmailServerConfiguration;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.utils.FormatUtilities;

import com.atrums.nomina.util.EmailManager;
import com.atrums.nomina.util.UtilNomina;

//import oracle.net.aso.e;

class NOCBPartner {
  private static final Logger log = Logger.getLogger(NOCBPartner.class);
  public static NOCBPartnerData[] emailTercero = null;
  private static NOCBPartnerData[] data = null;
  private static ConnectionProvider connectionProvider;

  public NOCBPartner(ConnectionProvider connectionProvider, NOCBPartnerData[] data) {
    try { //JULIO 2020
		setConnectionProvider(connectionProvider);
		setData(data);
	} catch (Exception ignore) {
		ignore.printStackTrace();
	}
  }

  public NOCBPartner(ConnectionProvider connectionProvider) {
    setConnectionProvider(connectionProvider);
  }

  public void testEnvios() throws Exception {
    try { //JULIO 2020
		enviarMail(getData());
	} catch (Exception ignore) {
		ignore.printStackTrace();
	}
  }

  public static NOCBPartnerData[] obtenerMailTerceros(String idTercero) throws ServletException {
    NOCBPartnerData[] datos = null;
    datos = NOCBPartnerData.select(getConnectionProvider(), idTercero);
    return datos;
  }

  public static NOCBPartnerData[] mailPagosServicios(String idTercero) throws ServletException {
    NOCBPartnerData[] datos = null;
    datos = NOCBPartnerData.selectPagoServicio(getConnectionProvider(), idTercero);
    return datos;
  }

  public Integer permitirEnviarMail() throws Exception {
    NOCBPartnerData[] datos = null;
    datos = NOCBPartnerData.selectEmployee(getConnectionProvider());
    return datos.length;
  }

  @SuppressWarnings("unused")
  public static void enviarMail(NOCBPartnerData[] datosEmail) throws Exception {
    String strBaseDesin = "@basedesign@/com/atrums/nomina/ad_reports/RPT_Rol_Pagos_Individual.jrxml";
    try {
      for (int i = 0; datosEmail.length > i; i++) {
    	Thread.sleep(60000); //JULIO 2020
        String periodo = datosEmail[i].getField("mes") + "  " + datosEmail[i].getField("anio");
        String idOrg = datosEmail[i].getField("organizacion");
        String recipient = datosEmail[i].getField("email");
        Organization currenctOrg = obtenerOrganizacion(idOrg);
        EmailServerConfiguration mailConfig = obtenerConfiguracionesMail(idOrg);
        Client cliente = obtenerCliente(idOrg); 
        log.info(mailConfig);
        String username = mailConfig.getSmtpServerAccount();
        // log.info(username);
        String password = FormatUtilities.encryptDecrypt(mailConfig.getSmtpServerPassword(), false);
        // log.info(password);
        String connSecurity = mailConfig.getSmtpConnectionSecurity();
        log.info(connSecurity);
        int port = mailConfig.getSmtpPort().intValue();
        log.info(port);
        String senderAddress = mailConfig.getSmtpServerSenderAddress();
        String host = mailConfig.getSmtpServer();
        boolean auth = mailConfig.isSMTPAuthentification();
        List<File> lisdoc = new ArrayList<File>();
        log.info("Fin de configuraciones..........");
        File flPdf = UtilNomina.generarPDF(connectionProvider, strBaseDesin, "rolpago",
            datosEmail[i].getField("rolpago"));
        lisdoc.add(flPdf);
        log.info("Aca deberia imprimirse el path.........");
        log.info(flPdf.getAbsolutePath());

        /*NPI 05-06-2020*/
        String strContenido = "";
        String type = "text/html; charset=utf-8";
        
        strContenido = "<table style=\"width: 85%; padding: 10px; margin:0 auto; border-collapse: collapse;font-family: sans-serif\">\r\n" + 
        		"	<tr style=\"background-color: #003764\">\r\n" + 
        		"		<td style=\"width: 85%\">\r\n" + 
        		"		    <a target=\"_blank\" href=\"http://atrums.com/\">\r\n" + 
        		"			   <img width=\"20%\" style=\"display:block; margin: 1.5% 3%\" src=\"http://ws.atrums.com/portal/images/publicidad/atrumsit-logo.png\">\r\n" + 
        		"			</a></td>\r\n" + 
        		"		<td style=\"width: 3%\">\r\n" + 
        		"			<a target=\"_blank\" href=\"https://www.facebook.com/Atrumsit-393603567398708/\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/facebook.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"        <td style=\"width: 3%\">		\r\n" + 
        		"			<a target=\"_blank\" href=\"https://www.instagram.com/atrumsit/?hl=es-la\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/instagram.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"        <td style=\"width: 3%\">		\r\n" + 
        		"			<a target=\"_blank\" href=\"https://www.youtube.com/channel/UCQ6Vl9DHMH3NNa93HxsqAUw?view_as=subscriber\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/youtube.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"        <td style=\"width: 3%\">	\r\n" + 
        		"			<a target=\"_blank\" href=\"https://twitter.com/atrumsit\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/twitter.png\"/>\r\n" + 
        		"			</a></td>	\r\n" + 
        		"		<td style=\"width: 3%\">\r\n" + 
        		"			<a target=\"_blank\" href=\"https://au.linkedin.com/company/atrumsit\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/linkedin.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"	</tr>\r\n" + 
        		"	<tr>\r\n" + 
        		"		<td style=\"background-color: #ECF0F1\" colspan=\"6\">\r\n" + 
        		"		  <div style=\"border: 6px solid #FFFFFF\">\r\n" + 
        		"			<div style=\"color: #34495e; margin: 2% 6% 2%; text-align: justify;font-size: 14px\">\r\n" + 
        		"				<h2 style=\"color: #8DC63F\">Estimad@:</h2>\r\n" + 
        		"				<p><b>"+ datosEmail[i].getField("tercero") +"</b></p>\r\n" + 
        		"				<p>Usted tiene un nuevo Rol de Pagos generado por la empresa <b>"+ cliente.getName().toString() +"</b>, referente al período "+ datosEmail[i].getField("mes") + "  "+ datosEmail[i].getField("anio") +".</p>\r\n" + 
        		"				<div style=\"width: 100%;text-align: center;margin-top: 38px\">\r\n" + 
        		"					Si quiere conocer más de nuestros servicios, haga clic aquí<br><br>\r\n" + 
        		"					<a style=\"text-decoration: none; border-radius: 5px; padding: 8px 18px; color: white; background-color: #8DC63F\" target=\"_blank\" href=\"http://atrums.com/\">ATRUMS IT</a>	\r\n" + 
        		"				</div>	\r\n" + 
        		"				<div style=\"font-size: 12px\">\r\n" + 
        		"					<p style=\"margin-top:50px\">\r\n" + 
        		"					   La información y archivos adjuntos contenidos en este mensaje electrónico son confidenciales y reservados; por tanto no pueden ser usados, reproducidos o divulgados por otras personas distintas a su(s) destinatario(s). \r\n" + 
        		"					   Si Ud. no es el destinatario de este email, le solicitamos comedidamente eliminarlo.\r\n" + 
        		"					</p>\r\n" + 
        		"					<p>\r\n" + 
        		"					   Cualquier novedad comunicarse con Talento Humano.\r\n" + 
        		"					<p>\r\n" +         		
        		"					<p>\r\n" + 
        		"					   Por favor, no responda a este correo electrónico.\r\n" + 
        		"					<p>\r\n" + 
        		"				</div>\r\n" + 
        		"			</div>\r\n" + 
        		"		  </div>\r\n" + 
        		"		</td>\r\n" + 
        		"	</tr>\r\n" + 
        		"	<tr>\r\n" + 
        		"	  <td colspan=\"6\">\r\n" + 
        		"		<table style=\"width: 100%\">\r\n" + 
        		"		    <tr style=\"color: #FFFFFF;background-color: #003764;height: 40px\">\r\n" + 
        		"				<td style = \"width: 100%;font-size: 13px\">\r\n" + 
        		"				    <div style=\"width:98%;margin: 1% 2% 1%\">\r\n" + 
        		"						<div><img style=\"width: 14px;\" src=\"http://ws.atrums.com/portal/images/publicidad/ubicacion.png\"/>\r\n" + 
        		"							 <a target=\"_blank\" href=\"http://atrums.com/\" style=\"color: #FFFFFF\">www.atrums.com</a>\r\n" + 
        		"						</div>\r\n" + 
        		"						<div><img style=\"width: 14px;\" src=\"http://ws.atrums.com/portal/images/publicidad/email.png\"/>\r\n" + 
        		"							 <a style=\"text-decoration: none;color: #FFFFFF\">info@atrums.com</a>\r\n" + 
        		"						</div>\r\n" + 
        		"						<div style=\"font-size: 12px\">\r\n" + 
        		"							 <img style=\"width: 10px;\" src=\"http://ws.atrums.com/portal/images/publicidad/telefono.png\"/>\r\n" + 
        		"							 022247848 | 022241461 | <a target=\"_blank\" href=\"https://web.whatsapp.com/\" style=\"color: #FFFFFF\">0958743792</a>\r\n" + 
        		"						</div>\r\n" + 
        		"					</div>\r\n" + 
        		"				</td>\r\n" + 
        		"			</tr>\r\n" + 
        		"		</table>\r\n" + 
        		"	  </td>\r\n" + 
        		"	</tr>\r\n" + 
        		"</table>";
        
        EmailManager
            .sendEmail(
                host,
                auth,
                username,
                password,
                connSecurity,
                port,
                senderAddress,
                recipient,
                null,
                null,
                null,
                "Entrega Rol de Pagos",
                strContenido, type, lisdoc, null,
                null);
      }
    }catch (Exception ex) {
      System.out.println(ex);
    }
  }

  public static void enviarMailPagoServicio(NOCBPartnerData[] datosEmail) throws Exception {
    String strBaseDesin = "@basedesign@/com/atrums/nomina/ad_reports/RPT_PagosServicios.jrxml";
    try {
      for (int i = 0; datosEmail.length > i; i++) {
    	Thread.sleep(60000); //JULIO 2020
        String periodo = datosEmail[i].getField("mes") + "  " + datosEmail[i].getField("anio");
        String idOrg = datosEmail[i].getField("organizacion");
        String recipient = datosEmail[i].getField("email");
        EmailServerConfiguration mailConfig = obtenerConfiguracionesMail(idOrg);
        log.info(mailConfig);
        String username = mailConfig.getSmtpServerAccount();
        String password = FormatUtilities.encryptDecrypt(mailConfig.getSmtpServerPassword(), false);
        String connSecurity = mailConfig.getSmtpConnectionSecurity();
        log.info(connSecurity);
        int port = mailConfig.getSmtpPort().intValue();
        log.info(port);
        String senderAddress = mailConfig.getSmtpServerSenderAddress();
        String host = mailConfig.getSmtpServer();
        boolean auth = mailConfig.isSMTPAuthentification();
        List<File> lisdoc = new ArrayList<File>();
        log.info("Fin de configuraciones..........");
        File flPdf = UtilNomina.generarPDF(connectionProvider, strBaseDesin, "pagoServicio",
            datosEmail[i].getField("rolpago"));
        lisdoc.add(flPdf);
        log.info("Aca deberia imprimirse el path.........");

        EmailManager
            .sendEmail(
                host,
                auth,
                username,
                password,
                connSecurity,
                port,
                senderAddress,
                recipient,
                null,
                null,
                null,
                "Entrega Pago Anticipo",
                "Estimado Colaborador buen día,"
                    + "\n\n El presente correo es para notificar el valor a recibir por Pago de Anticipo."
                    + "\n\n Detalle:\t" + periodo + "\n\n " + "\t\t Valor: "
                    + datosEmail[i].getField("valorPagoServicio") + " USD" + "\n\n\nAtentamente,"
                    + "\n" + "Departamento Recursos Humanos", null, lisdoc, null, null);
      }
    } catch (Exception ex) {
      System.out.println(ex);
    }

  }

  private static EmailServerConfiguration obtenerConfiguracionesMail(String organizacionId) {
    final EmailServerConfiguration o = getOne(EmailServerConfiguration.class, "select r from "
        + EmailServerConfiguration.class.getName() + " r where " + " r.client = "
        + "(select e.client from Organization e where lower(e.name) = lower('" + organizacionId + "'))");
    		System.out.println("QUERY MOD: " + o);
    return o;
  }

  private static Organization obtenerOrganizacion(String organizacionId) {
    final Organization o = getOne(Organization.class,
        "select r from " + Organization.class.getName() + " r where " + " r."
            + Organization.PROPERTY_ID + "='" + organizacionId + "'");
    return o;
  }
  
  private static Client obtenerCliente(String organizacionId) {
	    final Client o = getOne(Client.class,
	        "select c from Organization o inner join o.client c where lower(o.name) = lower('"+ organizacionId +"')");
	    return o;
  }

  private static <T extends Object> T getOne(Class<T> clz, String qryStr) {
    return getOne(clz, qryStr, true);
  }

  @SuppressWarnings({ "unchecked" })
  private static <T extends Object> T getOne(Class<T> clz, String qryStr, boolean doCheck) {
    final Query qry = SessionHandler.getInstance().createQuery(qryStr);
    qry.setMaxResults(1);
    final List<?> result = qry.list();
    if (doCheck && result.size() != 1) {
      log.error("The query '" + qryStr + "' returned " + result.size()
          + " results while only 1 result was expected");
    }
    if (result.size() == 0) {
      return null;
    }
    return (T) result.get(0);
  }

  public static NOCBPartnerData[] getData() {
    return data;
  }

  public static void setData(NOCBPartnerData[] data) {
    NOCBPartner.data = data;
  }

  public static ConnectionProvider getConnectionProvider() {
    return connectionProvider;
  }

  public static void setConnectionProvider(ConnectionProvider connectionProvider) {
    NOCBPartner.connectionProvider = connectionProvider;
  }
}
