//Sqlc generated V1.O00-1
package com.atrums.pedido.roles.process;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class ProcesarPedidoData implements FieldProvider {
static Logger log4j = Logger.getLogger(ProcesarPedidoData.class);
  private String InitRecordNumber="0";
  public String dato1;
  public String dato2;
  public String dato3;
  public String dato4;
  public String dato5;
  public String dato6;
  public String dato7;
  public String dato8;
  public String dato9;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("dato1"))
      return dato1;
    else if (fieldName.equalsIgnoreCase("dato2"))
      return dato2;
    else if (fieldName.equalsIgnoreCase("dato3"))
      return dato3;
    else if (fieldName.equalsIgnoreCase("dato4"))
      return dato4;
    else if (fieldName.equalsIgnoreCase("dato5"))
      return dato5;
    else if (fieldName.equalsIgnoreCase("dato6"))
      return dato6;
    else if (fieldName.equalsIgnoreCase("dato7"))
      return dato7;
    else if (fieldName.equalsIgnoreCase("dato8"))
      return dato8;
    else if (fieldName.equalsIgnoreCase("dato9"))
      return dato9;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static ProcesarPedidoData[] methodSeleccionardummy(ConnectionProvider connectionProvider)    throws ServletException {
    return methodSeleccionardummy(connectionProvider, 0, 0);
  }

  public static ProcesarPedidoData[] methodSeleccionardummy(ConnectionProvider connectionProvider, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select d.dummy as dato1, " +
      "             d.dummy as dato2, " +
      "             d.dummy as dato3, " +
      "             d.dummy as dato4," +
      "             d.dummy as dato5," +
      "             d.dummy as dato6," +
      "             d.dummy as dato7," +
      "             d.dummy as dato8," +
      "             d.dummy as dato9" +
      "      from dual d ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ProcesarPedidoData objectProcesarPedidoData = new ProcesarPedidoData();
        objectProcesarPedidoData.dato1 = UtilSql.getValue(result, "dato1");
        objectProcesarPedidoData.dato2 = UtilSql.getValue(result, "dato2");
        objectProcesarPedidoData.dato3 = UtilSql.getValue(result, "dato3");
        objectProcesarPedidoData.dato4 = UtilSql.getValue(result, "dato4");
        objectProcesarPedidoData.dato5 = UtilSql.getValue(result, "dato5");
        objectProcesarPedidoData.dato6 = UtilSql.getValue(result, "dato6");
        objectProcesarPedidoData.dato7 = UtilSql.getValue(result, "dato7");
        objectProcesarPedidoData.dato8 = UtilSql.getValue(result, "dato8");
        objectProcesarPedidoData.dato9 = UtilSql.getValue(result, "dato9");
        objectProcesarPedidoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectProcesarPedidoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ProcesarPedidoData objectProcesarPedidoData[] = new ProcesarPedidoData[vector.size()];
    vector.copyInto(objectProcesarPedidoData);
    return(objectProcesarPedidoData);
  }

  public static int methodInsertarTemp(ConnectionProvider connectionProvider, String ad_client_id, String amount, String tipopagot, String tipopagoc, String tipopagob, String nro_referencia, String nro_cheque, String fin_paymentmethod, String ad_client_id2, String c_order_id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  INSERT INTO prol_temporal_pago(" +
      "            prol_temporal_pago_id, ad_client_id, ad_org_id, isactive, created, " +
      "            createdby, updated, updatedby, amount, description, referenceno, " +
      "            nro_cheque, fin_paymentmethod_id, c_order_id)" +
      "      VALUES (get_uuid(), ?, '0', 'Y', now(), " +
      "            '100', now(), '100', CAST ( ? AS numeric ), (SELECT " +
      "            COALESCE((SELECT CASE WHEN at.nombre IS NOT NULL THEN ('Tarjeta de Credito: ' || at.nombre) ELSE '' END " +
      "            FROM prol_tarjeta at WHERE at.prol_tarjeta_id = ?), '') || " +
      "            COALESCE((SELECT CASE WHEN ac.name IS NOT NULL THEN (', Plan de Pago: ' || ac.name) ELSE '' END " +
      "            FROM prol_condicion ac WHERE ac.prol_condicion_id = ?), '') || " +
      "            COALESCE((SELECT CASE WHEN ab.nombre IS NOT NULL THEN ('Banco: ' || ab.nombre) ELSE '' END " +
      "            FROM prol_banco ab WHERE ab.prol_banco_id = ?), '') FROM dual), ?, " +
      "            ?, (SELECT fin_paymentmethod_id FROM fin_paymentmethod WHERE upper(name) LIKE ? AND isactive = 'Y' AND ad_org_id = '0' AND ad_client_id = ?), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_client_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipopagot);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipopagoc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipopagob);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nro_referencia);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nro_cheque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fin_paymentmethod);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_client_id2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_order_id);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int methodInsertarAnticipo(ConnectionProvider connectionProvider, String ad_client_id, String amount, String tipopagot, String tipopagob, String nro_referencia, String nro_cheque, String fin_paymentmethod, String c_order_id, String fin_payment_id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      INSERT INTO prol_anticipo_pago(" +
      "            prol_anticipo_pago_id, ad_client_id, ad_org_id, isactive, created, " +
      "            createdby, updated, updatedby, amount, description, referenceno, " +
      "            nro_cheque, fin_paymentmethod_id, c_order_id, fin_payment_id)" +
      "      VALUES (get_uuid(), ?, '0', 'Y', now(), " +
      "            '100', now(), '100', CAST ( ? AS numeric ), (SELECT COALESCE((SELECT CASE WHEN at.nombre IS NOT NULL THEN ('Tarjeta de Credito: ' || at.nombre) ELSE '' END FROM prol_tarjeta at WHERE at.prol_tarjeta_id = ?), '') || COALESCE((SELECT CASE WHEN ab.nombre IS NOT NULL THEN ('Banco: ' || ab.nombre) ELSE '' END FROM prol_banco ab WHERE ab.prol_banco_id = ?), '') FROM dual), ?, " +
      "            ?, (SELECT fin_paymentmethod_id FROM fin_paymentmethod WHERE upper(name) LIKE ? AND isactive = 'Y' AND ad_org_id = '0'), ?, ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_client_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipopagot);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipopagob);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nro_referencia);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nro_cheque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fin_paymentmethod);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_order_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fin_payment_id);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int methodUpdateOrder(ConnectionProvider connectionProvider, String estado, String c_order_id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      UPDATE c_order SET em_prol_docstatus = ? WHERE c_order_id = ?;";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_order_id);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int methodUpdateStatusAnticipo(ConnectionProvider connectionProvider, String estado, String c_order_id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      UPDATE c_order SET em_prol_anticipostatus = ? WHERE c_order_id = ?;";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_order_id);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int methodDeleteOrder(ConnectionProvider connectionProvider, String c_order_id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      DELETE FROM prol_temporal_pago WHERE c_order_id = ?;";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_order_id);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static ProcesarPedidoData[] methodSeleccionarNumLine(ConnectionProvider connectionProvider, String c_order_id)    throws ServletException {
    return methodSeleccionarNumLine(connectionProvider, c_order_id, 0, 0);
  }

  public static ProcesarPedidoData[] methodSeleccionarNumLine(ConnectionProvider connectionProvider, String c_order_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select (case when count(*) > 0 then 'Y' else 'N' end) as dato1 " +
      "	        from c_orderline where c_order_id = ?;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_order_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ProcesarPedidoData objectProcesarPedidoData = new ProcesarPedidoData();
        objectProcesarPedidoData.dato1 = UtilSql.getValue(result, "dato1");
        objectProcesarPedidoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectProcesarPedidoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ProcesarPedidoData objectProcesarPedidoData[] = new ProcesarPedidoData[vector.size()];
    vector.copyInto(objectProcesarPedidoData);
    return(objectProcesarPedidoData);
  }

  public static ProcesarPedidoData[] methodSeleccionarEnvio(ConnectionProvider connectionProvider, String ad_client_id)    throws ServletException {
    return methodSeleccionarEnvio(connectionProvider, ad_client_id, 0, 0);
  }

  public static ProcesarPedidoData[] methodSeleccionarEnvio(ConnectionProvider connectionProvider, String ad_client_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	            select c_order_id as dato1" +
      "				  from c_order" +
      "				 where issotrx = 'Y'" +
      "				   and em_prol_estado_pedido in ('PD','AP','VR','RZ','FC')" +
      "				   and em_prol_enviar = 'Y'" +
      "				   and ad_client_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_client_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ProcesarPedidoData objectProcesarPedidoData = new ProcesarPedidoData();
        objectProcesarPedidoData.dato1 = UtilSql.getValue(result, "dato1");
        objectProcesarPedidoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectProcesarPedidoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ProcesarPedidoData objectProcesarPedidoData[] = new ProcesarPedidoData[vector.size()];
    vector.copyInto(objectProcesarPedidoData);
    return(objectProcesarPedidoData);
  }

  public static int methodActualizaEnviar(ConnectionProvider connectionProvider, String c_order_id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	            update c_order" +
      "				   set em_prol_enviar = 'N'" +
      "				 where c_order_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_order_id);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static ProcesarPedidoData[] methodEnviarDatos(ConnectionProvider connectionProvider, String c_order_id)    throws ServletException {
    return methodEnviarDatos(connectionProvider, c_order_id, 0, 0);
  }

  public static ProcesarPedidoData[] methodEnviarDatos(ConnectionProvider connectionProvider, String c_order_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	            select a.documentno as dato1, " +
      "				       date(a.dateordered) as dato2, " +
      "				       b.name as dato3, " +
      "				       (d.firstname || ' ' || d.lastname) as dato4," +
      "				       a.grandtotal as dato5," +
      "				       (select string_agg(x,';')" +
      "				          from (" +
      "				               select string_agg(u.email,';') as x" +
      "				                  from ad_user u" +
      "				                 where u.c_bpartner_id = c.c_bpartner_id" +
      "				                   and u.em_atecfe_check_email = 'Y'" +
      "				               union all" +
      "				               select string_agg(v.email,';') as x" +
      "				                  from ad_user v" +
      "				                 where v.c_bpartner_id = e.c_bpartner_id" +
      "				                   and v.em_atecfe_check_email = 'Y'" +
      "				                ) as info" +
      "				                ) as dato6," +
      "				       (select name" +
      "				           from ad_ref_list" +
      "				          where ad_reference_id = '02BC73ABA84748BE822FBFCDB619A338'" +
      "				            and value = a.em_prol_estado_pedido) as dato7," +
      "				       f.name as dato8" +
      "				  from c_order a" +
      "				  left join c_bpartner b on (a.c_bpartner_id = b.c_bpartner_id)" +
      "				  left join c_bpartner c on (c.c_bpartner_id = b.salesrep_id)" +
      "				  left join ad_user d on (d.ad_user_id = a.createdby)  " +
      "				  left join c_bpartner e on (e.c_bpartner_id = d.c_bpartner_id)" +
      "				  left join ad_client f on (f.ad_client_id = a.ad_client_id)" +
      "				 where a.c_order_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_order_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ProcesarPedidoData objectProcesarPedidoData = new ProcesarPedidoData();
        objectProcesarPedidoData.dato1 = UtilSql.getValue(result, "dato1");
        objectProcesarPedidoData.dato2 = UtilSql.getDateValue(result, "dato2", "dd-MM-yyyy");
        objectProcesarPedidoData.dato3 = UtilSql.getValue(result, "dato3");
        objectProcesarPedidoData.dato4 = UtilSql.getValue(result, "dato4");
        objectProcesarPedidoData.dato5 = UtilSql.getValue(result, "dato5");
        objectProcesarPedidoData.dato6 = UtilSql.getValue(result, "dato6");
        objectProcesarPedidoData.dato7 = UtilSql.getValue(result, "dato7");
        objectProcesarPedidoData.dato8 = UtilSql.getValue(result, "dato8");
        objectProcesarPedidoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectProcesarPedidoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ProcesarPedidoData objectProcesarPedidoData[] = new ProcesarPedidoData[vector.size()];
    vector.copyInto(objectProcesarPedidoData);
    return(objectProcesarPedidoData);
  }
}
