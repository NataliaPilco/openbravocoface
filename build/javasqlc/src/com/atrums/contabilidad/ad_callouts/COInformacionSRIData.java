//Sqlc generated V1.O00-1
package com.atrums.contabilidad.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class COInformacionSRIData implements FieldProvider {
static Logger log4j = Logger.getLogger(COInformacionSRIData.class);
  private String InitRecordNumber="0";
  public String estab;
  public String emision;
  public String noautoriza;
  public String fecautoriza;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("estab"))
      return estab;
    else if (fieldName.equalsIgnoreCase("emision"))
      return emision;
    else if (fieldName.equalsIgnoreCase("noautoriza"))
      return noautoriza;
    else if (fieldName.equalsIgnoreCase("fecautoriza"))
      return fecautoriza;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static COInformacionSRIData[] selectBP(ConnectionProvider connectionProvider, String strbpartnerId)    throws ServletException {
    return selectBP(connectionProvider, strbpartnerId, 0, 0);
  }

  public static COInformacionSRIData[] selectBP(ConnectionProvider connectionProvider, String strbpartnerId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "            select COALESCE(em_co_bp_nro_estab,'001') as estab, " +
      "                   COALESCE(em_co_bp_punto_emision,'001') as emision," +
      "                   COALESCE(em_co_bp_nro_aut_fc_sri,'') as noAutoriza, " +
      "                   em_co_bp_fec_venct_aut_fc_sri as fecAutoriza" +
      "             from c_bpartner  " +
      "            where c_bpartner_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strbpartnerId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        COInformacionSRIData objectCOInformacionSRIData = new COInformacionSRIData();
        objectCOInformacionSRIData.estab = UtilSql.getValue(result, "estab");
        objectCOInformacionSRIData.emision = UtilSql.getValue(result, "emision");
        objectCOInformacionSRIData.noautoriza = UtilSql.getValue(result, "noautoriza");
        objectCOInformacionSRIData.fecautoriza = UtilSql.getDateValue(result, "fecautoriza", "dd-MM-yyyy");
        objectCOInformacionSRIData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCOInformacionSRIData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    COInformacionSRIData objectCOInformacionSRIData[] = new COInformacionSRIData[vector.size()];
    vector.copyInto(objectCOInformacionSRIData);
    return(objectCOInformacionSRIData);
  }

  public static COInformacionSRIData[] selectIv(ConnectionProvider connectionProvider, String strInvoiceId)    throws ServletException {
    return selectIv(connectionProvider, strInvoiceId, 0, 0);
  }

  public static COInformacionSRIData[] selectIv(ConnectionProvider connectionProvider, String strInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select COALESCE(em_co_bp_nro_estab,'001') as estab, " +
      "               COALESCE(em_co_bp_punto_emision,'001') as emision," +
      "               COALESCE(em_co_bp_nro_aut_rt_sri,'') as noAutoriza, " +
      "               em_co_bp_fec_venct_aut_rt_sri as fecAutoriza" +
      "         from c_bpartner  " +
      "        where c_bpartner_id in (select c_bpartner_id from c_invoice where c_invoice_id = ?)";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        COInformacionSRIData objectCOInformacionSRIData = new COInformacionSRIData();
        objectCOInformacionSRIData.estab = UtilSql.getValue(result, "estab");
        objectCOInformacionSRIData.emision = UtilSql.getValue(result, "emision");
        objectCOInformacionSRIData.noautoriza = UtilSql.getValue(result, "noautoriza");
        objectCOInformacionSRIData.fecautoriza = UtilSql.getDateValue(result, "fecautoriza", "dd-MM-yyyy");
        objectCOInformacionSRIData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCOInformacionSRIData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    COInformacionSRIData objectCOInformacionSRIData[] = new COInformacionSRIData[vector.size()];
    vector.copyInto(objectCOInformacionSRIData);
    return(objectCOInformacionSRIData);
  }

  public static int updateInvoice(ConnectionProvider connectionProvider, String strEstab, String strEmision, String strNoAutorizaFac, String strFecVencAutFac, String strBpartnerId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "            update c_bpartner " +
      "               set em_co_bp_nro_estab = ?," +
      "                   em_co_bp_punto_emision = ?," +
      "                   em_co_bp_nro_aut_fc_sri = ?," +
      "                   em_co_bp_fec_venct_aut_fc_sri=?" +
      "            where c_bpartner_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strEstab);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strNoAutorizaFac);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strFecVencAutFac);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strBpartnerId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int updateRetencion(ConnectionProvider connectionProvider, String strEstab, String strEmision, String strNoAutorizaRect, String strFecVencAutRect, String strInvoiceId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "            update c_bpartner " +
      "               set em_co_bp_nro_estab = ?," +
      "                   em_co_bp_punto_emision = ?," +
      "                   em_co_bp_nro_aut_rt_sri = ?," +
      "                   em_co_bp_fec_venct_aut_rt_sri = ?" +
      "            where c_bpartner_id in (select c_bpartner_id from c_invoice where c_invoice_id = ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strEstab);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strNoAutorizaRect);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strFecVencAutRect);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strInvoiceId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }
}
