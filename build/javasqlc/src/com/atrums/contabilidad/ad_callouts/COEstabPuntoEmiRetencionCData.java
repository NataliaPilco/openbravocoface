//Sqlc generated V1.O00-1
package com.atrums.contabilidad.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class COEstabPuntoEmiRetencionCData implements FieldProvider {
static Logger log4j = Logger.getLogger(COEstabPuntoEmiRetencionCData.class);
  private String InitRecordNumber="0";
  public String establecimiento;
  public String emision;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("establecimiento"))
      return establecimiento;
    else if (fieldName.equalsIgnoreCase("emision"))
      return emision;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static COEstabPuntoEmiRetencionCData[] select(ConnectionProvider connectionProvider, String strOrg)    throws ServletException {
    return select(connectionProvider, strOrg, 0, 0);
  }

  public static COEstabPuntoEmiRetencionCData[] select(ConnectionProvider connectionProvider, String strOrg, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "          select COALESCE(o.em_co_nro_estab,'001') as establecimiento, " +
      "                 COALESCE(o.em_co_punto_emision,'001') as emision " +
      "            from ad_org o" +
      "           where o.ad_org_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strOrg);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        COEstabPuntoEmiRetencionCData objectCOEstabPuntoEmiRetencionCData = new COEstabPuntoEmiRetencionCData();
        objectCOEstabPuntoEmiRetencionCData.establecimiento = UtilSql.getValue(result, "establecimiento");
        objectCOEstabPuntoEmiRetencionCData.emision = UtilSql.getValue(result, "emision");
        objectCOEstabPuntoEmiRetencionCData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCOEstabPuntoEmiRetencionCData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    COEstabPuntoEmiRetencionCData objectCOEstabPuntoEmiRetencionCData[] = new COEstabPuntoEmiRetencionCData[vector.size()];
    vector.copyInto(objectCOEstabPuntoEmiRetencionCData);
    return(objectCOEstabPuntoEmiRetencionCData);
  }
}
