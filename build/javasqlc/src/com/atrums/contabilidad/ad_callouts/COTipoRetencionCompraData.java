//Sqlc generated V1.O00-1
package com.atrums.contabilidad.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class COTipoRetencionCompraData implements FieldProvider {
static Logger log4j = Logger.getLogger(COTipoRetencionCompraData.class);
  private String InitRecordNumber="0";
  public String coBpRetencionCompraId;
  public String descripcion;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("co_bp_retencion_compra_id") || fieldName.equals("coBpRetencionCompraId"))
      return coBpRetencionCompraId;
    else if (fieldName.equalsIgnoreCase("descripcion"))
      return descripcion;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static COTipoRetencionCompraData[] select(ConnectionProvider connectionProvider, String coRetencionCompraId, String cTipo)    throws ServletException {
    return select(connectionProvider, coRetencionCompraId, cTipo, 0, 0);
  }

  public static COTipoRetencionCompraData[] select(ConnectionProvider connectionProvider, String coRetencionCompraId, String cTipo, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	select bprc.co_bp_retencion_compra_id, bprc.descripcion" +
      "	  from co_retencion_compra rc, " +
      "		   c_invoice i, " +
      "		   co_bp_retencion_compra bprc, " +
      "		   co_tipo_retencion tr" +
      "	 where i.c_invoice_id = rc.c_invoice_id" +
      "	   and bprc.c_bpartner_id = i.c_bpartner_id" +
      "	   and bprc.co_tipo_retencion_id = tr.co_tipo_retencion_id" +
      "	   and rc.co_retencion_compra_id = ?  " +
      "	   and tr.tipo = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coRetencionCompraId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cTipo);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        COTipoRetencionCompraData objectCOTipoRetencionCompraData = new COTipoRetencionCompraData();
        objectCOTipoRetencionCompraData.coBpRetencionCompraId = UtilSql.getValue(result, "co_bp_retencion_compra_id");
        objectCOTipoRetencionCompraData.descripcion = UtilSql.getValue(result, "descripcion");
        objectCOTipoRetencionCompraData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCOTipoRetencionCompraData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    COTipoRetencionCompraData objectCOTipoRetencionCompraData[] = new COTipoRetencionCompraData[vector.size()];
    vector.copyInto(objectCOTipoRetencionCompraData);
    return(objectCOTipoRetencionCompraData);
  }

  public static COTipoRetencionCompraData[] selectRetenCompra(ConnectionProvider connectionProvider, String coRetencionCompraId, String cTipo)    throws ServletException {
    return selectRetenCompra(connectionProvider, coRetencionCompraId, cTipo, 0, 0);
  }

  public static COTipoRetencionCompraData[] selectRetenCompra(ConnectionProvider connectionProvider, String coRetencionCompraId, String cTipo, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	select bprc.co_bp_retencion_compra_id, bprc.descripcion" +
      "	  from co_retencion_compra rc, " +
      "		   co_bp_retencion_compra bprc, " +
      "		   co_tipo_retencion tr" +
      "	 where rc.c_bpartner_id = bprc.c_bpartner_id" +
      "	   and bprc.co_tipo_retencion_id = tr.co_tipo_retencion_id" +
      "	   and rc.co_retencion_compra_id = ?" +
      "	   and tr.tipo = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coRetencionCompraId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cTipo);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        COTipoRetencionCompraData objectCOTipoRetencionCompraData = new COTipoRetencionCompraData();
        objectCOTipoRetencionCompraData.coBpRetencionCompraId = UtilSql.getValue(result, "co_bp_retencion_compra_id");
        objectCOTipoRetencionCompraData.descripcion = UtilSql.getValue(result, "descripcion");
        objectCOTipoRetencionCompraData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCOTipoRetencionCompraData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    COTipoRetencionCompraData objectCOTipoRetencionCompraData[] = new COTipoRetencionCompraData[vector.size()];
    vector.copyInto(objectCOTipoRetencionCompraData);
    return(objectCOTipoRetencionCompraData);
  }
}
