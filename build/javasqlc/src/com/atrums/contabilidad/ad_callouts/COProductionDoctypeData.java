//Sqlc generated V1.O00-1
package com.atrums.contabilidad.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class COProductionDoctypeData implements FieldProvider {
static Logger log4j = Logger.getLogger(COProductionDoctypeData.class);
  private String InitRecordNumber="0";
  public String currentnext;
  public String prefix;
  public String suffix;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("currentnext"))
      return currentnext;
    else if (fieldName.equalsIgnoreCase("prefix"))
      return prefix;
    else if (fieldName.equalsIgnoreCase("suffix"))
      return suffix;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static COProductionDoctypeData[] select(ConnectionProvider connectionProvider, String cDocTypeId)    throws ServletException {
    return select(connectionProvider, cDocTypeId, 0, 0);
  }

  public static COProductionDoctypeData[] select(ConnectionProvider connectionProvider, String cDocTypeId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select LPAD((to_char(b.currentnext)), 5, '0') as currentnext, b.prefix, b.suffix" +
      "             from c_doctype a, ad_sequence b" +
      "             where a.docnosequence_id = b.ad_sequence_id" +
      "             and a.c_doctype_id = ?;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDocTypeId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        COProductionDoctypeData objectCOProductionDoctypeData = new COProductionDoctypeData();
        objectCOProductionDoctypeData.currentnext = UtilSql.getValue(result, "currentnext");
        objectCOProductionDoctypeData.prefix = UtilSql.getValue(result, "prefix");
        objectCOProductionDoctypeData.suffix = UtilSql.getValue(result, "suffix");
        objectCOProductionDoctypeData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCOProductionDoctypeData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    COProductionDoctypeData objectCOProductionDoctypeData[] = new COProductionDoctypeData[vector.size()];
    vector.copyInto(objectCOProductionDoctypeData);
    return(objectCOProductionDoctypeData);
  }
}
