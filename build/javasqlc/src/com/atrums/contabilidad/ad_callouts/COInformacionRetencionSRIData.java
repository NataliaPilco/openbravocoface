//Sqlc generated V1.O00-1
package com.atrums.contabilidad.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class COInformacionRetencionSRIData implements FieldProvider {
static Logger log4j = Logger.getLogger(COInformacionRetencionSRIData.class);
  private String InitRecordNumber="0";
  public String estab;
  public String emision;
  public String noautoriza;
  public String fecautoriza;
  public String taxid;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("estab"))
      return estab;
    else if (fieldName.equalsIgnoreCase("emision"))
      return emision;
    else if (fieldName.equalsIgnoreCase("noautoriza"))
      return noautoriza;
    else if (fieldName.equalsIgnoreCase("fecautoriza"))
      return fecautoriza;
    else if (fieldName.equalsIgnoreCase("taxid"))
      return taxid;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static COInformacionRetencionSRIData[] selectBP(ConnectionProvider connectionProvider, String strbpartnerId)    throws ServletException {
    return selectBP(connectionProvider, strbpartnerId, 0, 0);
  }

  public static COInformacionRetencionSRIData[] selectBP(ConnectionProvider connectionProvider, String strbpartnerId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "            select COALESCE(em_co_bp_nro_estab,'001') as estab, " +
      "                   COALESCE(em_co_bp_punto_emision,'001') as emision," +
      "                   COALESCE(em_co_bp_nro_aut_fc_sri,'') as noAutoriza, " +
      "                   em_co_bp_fec_venct_aut_fc_sri as fecAutoriza," +
      "                   taxid as taxid" +
      "             from c_bpartner  " +
      "            where c_bpartner_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strbpartnerId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        COInformacionRetencionSRIData objectCOInformacionRetencionSRIData = new COInformacionRetencionSRIData();
        objectCOInformacionRetencionSRIData.estab = UtilSql.getValue(result, "estab");
        objectCOInformacionRetencionSRIData.emision = UtilSql.getValue(result, "emision");
        objectCOInformacionRetencionSRIData.noautoriza = UtilSql.getValue(result, "noautoriza");
        objectCOInformacionRetencionSRIData.fecautoriza = UtilSql.getDateValue(result, "fecautoriza", "dd-MM-yyyy");
        objectCOInformacionRetencionSRIData.taxid = UtilSql.getValue(result, "taxid");
        objectCOInformacionRetencionSRIData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCOInformacionRetencionSRIData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    COInformacionRetencionSRIData objectCOInformacionRetencionSRIData[] = new COInformacionRetencionSRIData[vector.size()];
    vector.copyInto(objectCOInformacionRetencionSRIData);
    return(objectCOInformacionRetencionSRIData);
  }

  public static COInformacionRetencionSRIData[] selectIv(ConnectionProvider connectionProvider, String strBpartnerId)    throws ServletException {
    return selectIv(connectionProvider, strBpartnerId, 0, 0);
  }

  public static COInformacionRetencionSRIData[] selectIv(ConnectionProvider connectionProvider, String strBpartnerId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select COALESCE(em_co_bp_nro_estab,'001') as estab, " +
      "               COALESCE(em_co_bp_punto_emision,'001') as emision," +
      "               COALESCE(em_co_bp_nro_aut_rt_sri,'') as noAutoriza, " +
      "               em_co_bp_fec_venct_aut_rt_sri as fecAutoriza," +
      "               taxid as taxid" +
      "         from c_bpartner  " +
      "        where c_bpartner_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strBpartnerId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        COInformacionRetencionSRIData objectCOInformacionRetencionSRIData = new COInformacionRetencionSRIData();
        objectCOInformacionRetencionSRIData.estab = UtilSql.getValue(result, "estab");
        objectCOInformacionRetencionSRIData.emision = UtilSql.getValue(result, "emision");
        objectCOInformacionRetencionSRIData.noautoriza = UtilSql.getValue(result, "noautoriza");
        objectCOInformacionRetencionSRIData.fecautoriza = UtilSql.getDateValue(result, "fecautoriza", "dd-MM-yyyy");
        objectCOInformacionRetencionSRIData.taxid = UtilSql.getValue(result, "taxid");
        objectCOInformacionRetencionSRIData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCOInformacionRetencionSRIData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    COInformacionRetencionSRIData objectCOInformacionRetencionSRIData[] = new COInformacionRetencionSRIData[vector.size()];
    vector.copyInto(objectCOInformacionRetencionSRIData);
    return(objectCOInformacionRetencionSRIData);
  }

  public static int updateInvoice(ConnectionProvider connectionProvider, String strEstab, String strEmision, String strNoAutorizaFac, String strFecVencAutFac, String strRucBp, String strBpartnerId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "            update c_bpartner " +
      "               set em_co_bp_nro_estab = ?," +
      "                   em_co_bp_punto_emision = ?," +
      "                   em_co_bp_nro_aut_fc_sri = ?," +
      "                   em_co_bp_fec_venct_aut_fc_sri=?," +
      "                   taxid = ?" +
      "            where c_bpartner_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strEstab);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strNoAutorizaFac);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strFecVencAutFac);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strRucBp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strBpartnerId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }
}
