//Sqlc generated V1.O00-1
package com.atrums.clonar.factura.process;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class CopiarDocumentoData implements FieldProvider {
static Logger log4j = Logger.getLogger(CopiarDocumentoData.class);
  private String InitRecordNumber="0";
  public String dato1;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("dato1"))
      return dato1;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static CopiarDocumentoData[] methodSeleccionardummy(ConnectionProvider connectionProvider, String param1)    throws ServletException {
    return methodSeleccionardummy(connectionProvider, param1, 0, 0);
  }

  public static CopiarDocumentoData[] methodSeleccionardummy(ConnectionProvider connectionProvider, String param1, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  SELECT d.dummy as dato1" +
      "	  FROM dual d " +
      "	  WHERE d.dummy = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CopiarDocumentoData objectCopiarDocumentoData = new CopiarDocumentoData();
        objectCopiarDocumentoData.dato1 = UtilSql.getValue(result, "dato1");
        objectCopiarDocumentoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCopiarDocumentoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CopiarDocumentoData objectCopiarDocumentoData[] = new CopiarDocumentoData[vector.size()];
    vector.copyInto(objectCopiarDocumentoData);
    return(objectCopiarDocumentoData);
  }

  public static CopiarDocumentoData[] methodCrearDocumento(ConnectionProvider connectionProvider, String cInvoiceId, String cDoctypeId)    throws ServletException {
    return methodCrearDocumento(connectionProvider, cInvoiceId, cDoctypeId, 0, 0);
  }

  public static CopiarDocumentoData[] methodCrearDocumento(ConnectionProvider connectionProvider, String cInvoiceId, String cDoctypeId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT cf_crear_documento(?, ?) as dato1  " +
      "      FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CopiarDocumentoData objectCopiarDocumentoData = new CopiarDocumentoData();
        objectCopiarDocumentoData.dato1 = UtilSql.getValue(result, "dato1");
        objectCopiarDocumentoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCopiarDocumentoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CopiarDocumentoData objectCopiarDocumentoData[] = new CopiarDocumentoData[vector.size()];
    vector.copyInto(objectCopiarDocumentoData);
    return(objectCopiarDocumentoData);
  }
}
