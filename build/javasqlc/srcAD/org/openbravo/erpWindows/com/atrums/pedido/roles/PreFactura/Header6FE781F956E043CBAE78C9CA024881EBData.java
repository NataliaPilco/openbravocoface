//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.pedido.roles.PreFactura;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class Header6FE781F956E043CBAE78C9CA024881EBData implements FieldProvider {
static Logger log4j = Logger.getLogger(Header6FE781F956E043CBAE78C9CA024881EBData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String cDoctypetargetId;
  public String cDoctypetargetIdr;
  public String cReturnReasonId;
  public String documentno;
  public String dateordered;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String cBpartnerLocationId;
  public String cBpartnerLocationIdr;
  public String rmPickfromshipment;
  public String mPricelistId;
  public String mPricelistIdr;
  public String rmReceivematerials;
  public String datepromised;
  public String finPaymentmethodId;
  public String finPaymentmethodIdr;
  public String rmCreateinvoice;
  public String cPaymenttermId;
  public String cPaymenttermIdr;
  public String mWarehouseId;
  public String invoicerule;
  public String invoiceruler;
  public String deliveryviarule;
  public String deliveryviaruler;
  public String mShipperId;
  public String mShipperIdr;
  public String description;
  public String emProlCatProId;
  public String emProlCatProIdr;
  public String docstatus;
  public String grandtotal;
  public String totallines;
  public String cCurrencyId;
  public String emProlEstadoPedido;
  public String emProlEstadoPedidor;
  public String deliverynotes;
  public String emProlSectorActividadId;
  public String emProlBroker;
  public String emProlCatalogoBrokersId;
  public String emProlFechaSuscripcion;
  public String emProlDirecto;
  public String adUserId;
  public String salesrepId;
  public String cDoctypeId;
  public String soResStatus;
  public String poreference;
  public String billtoId;
  public String deliveryLocationId;
  public String emAprmAddpayment;
  public String docaction;
  public String copyfrom;
  public String copyfrompo;
  public String deliveryrule;
  public String freightcostrule;
  public String freightamt;
  public String isdiscountprinted;
  public String priorityrule;
  public String cCampaignId;
  public String chargeamt;
  public String cChargeId;
  public String cActivityId;
  public String adOrgtrxId;
  public String quotationId;
  public String calculatePromotions;
  public String rmAddorphanline;
  public String convertquotation;
  public String cRejectReasonId;
  public String validuntil;
  public String cProjectId;
  public String cProjectIdr;
  public String cCostcenterId;
  public String aAssetId;
  public String user1Id;
  public String user2Id;
  public String emProlFechaIniPoliza;
  public String emProlFechaFinPoliza;
  public String emProlNumPoliza;
  public String emProlSumAsegurada;
  public String emProlEnviarPedido;
  public String emProlProcesar;
  public String emProlProcesarBtn;
  public String emProlValidar;
  public String dateacct;
  public String processing;
  public String adClientId;
  public String issotrx;
  public String dropshipUserId;
  public String cIncotermsId;
  public String isactive;
  public String istaxincluded;
  public String isprinted;
  public String generatetemplate;
  public String isselfservice;
  public String paymentrule;
  public String incotermsdescription;
  public String isselected;
  public String isdelivered;
  public String processed;
  public String dropshipLocationId;
  public String isinvoiced;
  public String posted;
  public String cOrderId;
  public String dateprinted;
  public String dropshipBpartnerId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("c_doctypetarget_id") || fieldName.equals("cDoctypetargetId"))
      return cDoctypetargetId;
    else if (fieldName.equalsIgnoreCase("c_doctypetarget_idr") || fieldName.equals("cDoctypetargetIdr"))
      return cDoctypetargetIdr;
    else if (fieldName.equalsIgnoreCase("c_return_reason_id") || fieldName.equals("cReturnReasonId"))
      return cReturnReasonId;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("dateordered"))
      return dateordered;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_id") || fieldName.equals("cBpartnerLocationId"))
      return cBpartnerLocationId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_idr") || fieldName.equals("cBpartnerLocationIdr"))
      return cBpartnerLocationIdr;
    else if (fieldName.equalsIgnoreCase("rm_pickfromshipment") || fieldName.equals("rmPickfromshipment"))
      return rmPickfromshipment;
    else if (fieldName.equalsIgnoreCase("m_pricelist_id") || fieldName.equals("mPricelistId"))
      return mPricelistId;
    else if (fieldName.equalsIgnoreCase("m_pricelist_idr") || fieldName.equals("mPricelistIdr"))
      return mPricelistIdr;
    else if (fieldName.equalsIgnoreCase("rm_receivematerials") || fieldName.equals("rmReceivematerials"))
      return rmReceivematerials;
    else if (fieldName.equalsIgnoreCase("datepromised"))
      return datepromised;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_id") || fieldName.equals("finPaymentmethodId"))
      return finPaymentmethodId;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_idr") || fieldName.equals("finPaymentmethodIdr"))
      return finPaymentmethodIdr;
    else if (fieldName.equalsIgnoreCase("rm_createinvoice") || fieldName.equals("rmCreateinvoice"))
      return rmCreateinvoice;
    else if (fieldName.equalsIgnoreCase("c_paymentterm_id") || fieldName.equals("cPaymenttermId"))
      return cPaymenttermId;
    else if (fieldName.equalsIgnoreCase("c_paymentterm_idr") || fieldName.equals("cPaymenttermIdr"))
      return cPaymenttermIdr;
    else if (fieldName.equalsIgnoreCase("m_warehouse_id") || fieldName.equals("mWarehouseId"))
      return mWarehouseId;
    else if (fieldName.equalsIgnoreCase("invoicerule"))
      return invoicerule;
    else if (fieldName.equalsIgnoreCase("invoiceruler"))
      return invoiceruler;
    else if (fieldName.equalsIgnoreCase("deliveryviarule"))
      return deliveryviarule;
    else if (fieldName.equalsIgnoreCase("deliveryviaruler"))
      return deliveryviaruler;
    else if (fieldName.equalsIgnoreCase("m_shipper_id") || fieldName.equals("mShipperId"))
      return mShipperId;
    else if (fieldName.equalsIgnoreCase("m_shipper_idr") || fieldName.equals("mShipperIdr"))
      return mShipperIdr;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("em_prol_cat_pro_id") || fieldName.equals("emProlCatProId"))
      return emProlCatProId;
    else if (fieldName.equalsIgnoreCase("em_prol_cat_pro_idr") || fieldName.equals("emProlCatProIdr"))
      return emProlCatProIdr;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("grandtotal"))
      return grandtotal;
    else if (fieldName.equalsIgnoreCase("totallines"))
      return totallines;
    else if (fieldName.equalsIgnoreCase("c_currency_id") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("em_prol_estado_pedido") || fieldName.equals("emProlEstadoPedido"))
      return emProlEstadoPedido;
    else if (fieldName.equalsIgnoreCase("em_prol_estado_pedidor") || fieldName.equals("emProlEstadoPedidor"))
      return emProlEstadoPedidor;
    else if (fieldName.equalsIgnoreCase("deliverynotes"))
      return deliverynotes;
    else if (fieldName.equalsIgnoreCase("em_prol_sector_actividad_id") || fieldName.equals("emProlSectorActividadId"))
      return emProlSectorActividadId;
    else if (fieldName.equalsIgnoreCase("em_prol_broker") || fieldName.equals("emProlBroker"))
      return emProlBroker;
    else if (fieldName.equalsIgnoreCase("em_prol_catalogo_brokers_id") || fieldName.equals("emProlCatalogoBrokersId"))
      return emProlCatalogoBrokersId;
    else if (fieldName.equalsIgnoreCase("em_prol_fecha_suscripcion") || fieldName.equals("emProlFechaSuscripcion"))
      return emProlFechaSuscripcion;
    else if (fieldName.equalsIgnoreCase("em_prol_directo") || fieldName.equals("emProlDirecto"))
      return emProlDirecto;
    else if (fieldName.equalsIgnoreCase("ad_user_id") || fieldName.equals("adUserId"))
      return adUserId;
    else if (fieldName.equalsIgnoreCase("salesrep_id") || fieldName.equals("salesrepId"))
      return salesrepId;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("so_res_status") || fieldName.equals("soResStatus"))
      return soResStatus;
    else if (fieldName.equalsIgnoreCase("poreference"))
      return poreference;
    else if (fieldName.equalsIgnoreCase("billto_id") || fieldName.equals("billtoId"))
      return billtoId;
    else if (fieldName.equalsIgnoreCase("delivery_location_id") || fieldName.equals("deliveryLocationId"))
      return deliveryLocationId;
    else if (fieldName.equalsIgnoreCase("em_aprm_addpayment") || fieldName.equals("emAprmAddpayment"))
      return emAprmAddpayment;
    else if (fieldName.equalsIgnoreCase("docaction"))
      return docaction;
    else if (fieldName.equalsIgnoreCase("copyfrom"))
      return copyfrom;
    else if (fieldName.equalsIgnoreCase("copyfrompo"))
      return copyfrompo;
    else if (fieldName.equalsIgnoreCase("deliveryrule"))
      return deliveryrule;
    else if (fieldName.equalsIgnoreCase("freightcostrule"))
      return freightcostrule;
    else if (fieldName.equalsIgnoreCase("freightamt"))
      return freightamt;
    else if (fieldName.equalsIgnoreCase("isdiscountprinted"))
      return isdiscountprinted;
    else if (fieldName.equalsIgnoreCase("priorityrule"))
      return priorityrule;
    else if (fieldName.equalsIgnoreCase("c_campaign_id") || fieldName.equals("cCampaignId"))
      return cCampaignId;
    else if (fieldName.equalsIgnoreCase("chargeamt"))
      return chargeamt;
    else if (fieldName.equalsIgnoreCase("c_charge_id") || fieldName.equals("cChargeId"))
      return cChargeId;
    else if (fieldName.equalsIgnoreCase("c_activity_id") || fieldName.equals("cActivityId"))
      return cActivityId;
    else if (fieldName.equalsIgnoreCase("ad_orgtrx_id") || fieldName.equals("adOrgtrxId"))
      return adOrgtrxId;
    else if (fieldName.equalsIgnoreCase("quotation_id") || fieldName.equals("quotationId"))
      return quotationId;
    else if (fieldName.equalsIgnoreCase("calculate_promotions") || fieldName.equals("calculatePromotions"))
      return calculatePromotions;
    else if (fieldName.equalsIgnoreCase("rm_addorphanline") || fieldName.equals("rmAddorphanline"))
      return rmAddorphanline;
    else if (fieldName.equalsIgnoreCase("convertquotation"))
      return convertquotation;
    else if (fieldName.equalsIgnoreCase("c_reject_reason_id") || fieldName.equals("cRejectReasonId"))
      return cRejectReasonId;
    else if (fieldName.equalsIgnoreCase("validuntil"))
      return validuntil;
    else if (fieldName.equalsIgnoreCase("c_project_id") || fieldName.equals("cProjectId"))
      return cProjectId;
    else if (fieldName.equalsIgnoreCase("c_project_idr") || fieldName.equals("cProjectIdr"))
      return cProjectIdr;
    else if (fieldName.equalsIgnoreCase("c_costcenter_id") || fieldName.equals("cCostcenterId"))
      return cCostcenterId;
    else if (fieldName.equalsIgnoreCase("a_asset_id") || fieldName.equals("aAssetId"))
      return aAssetId;
    else if (fieldName.equalsIgnoreCase("user1_id") || fieldName.equals("user1Id"))
      return user1Id;
    else if (fieldName.equalsIgnoreCase("user2_id") || fieldName.equals("user2Id"))
      return user2Id;
    else if (fieldName.equalsIgnoreCase("em_prol_fecha_ini_poliza") || fieldName.equals("emProlFechaIniPoliza"))
      return emProlFechaIniPoliza;
    else if (fieldName.equalsIgnoreCase("em_prol_fecha_fin_poliza") || fieldName.equals("emProlFechaFinPoliza"))
      return emProlFechaFinPoliza;
    else if (fieldName.equalsIgnoreCase("em_prol_num_poliza") || fieldName.equals("emProlNumPoliza"))
      return emProlNumPoliza;
    else if (fieldName.equalsIgnoreCase("em_prol_sum_asegurada") || fieldName.equals("emProlSumAsegurada"))
      return emProlSumAsegurada;
    else if (fieldName.equalsIgnoreCase("em_prol_enviar_pedido") || fieldName.equals("emProlEnviarPedido"))
      return emProlEnviarPedido;
    else if (fieldName.equalsIgnoreCase("em_prol_procesar") || fieldName.equals("emProlProcesar"))
      return emProlProcesar;
    else if (fieldName.equalsIgnoreCase("em_prol_procesar_btn") || fieldName.equals("emProlProcesarBtn"))
      return emProlProcesarBtn;
    else if (fieldName.equalsIgnoreCase("em_prol_validar") || fieldName.equals("emProlValidar"))
      return emProlValidar;
    else if (fieldName.equalsIgnoreCase("dateacct"))
      return dateacct;
    else if (fieldName.equalsIgnoreCase("processing"))
      return processing;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("issotrx"))
      return issotrx;
    else if (fieldName.equalsIgnoreCase("dropship_user_id") || fieldName.equals("dropshipUserId"))
      return dropshipUserId;
    else if (fieldName.equalsIgnoreCase("c_incoterms_id") || fieldName.equals("cIncotermsId"))
      return cIncotermsId;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("istaxincluded"))
      return istaxincluded;
    else if (fieldName.equalsIgnoreCase("isprinted"))
      return isprinted;
    else if (fieldName.equalsIgnoreCase("generatetemplate"))
      return generatetemplate;
    else if (fieldName.equalsIgnoreCase("isselfservice"))
      return isselfservice;
    else if (fieldName.equalsIgnoreCase("paymentrule"))
      return paymentrule;
    else if (fieldName.equalsIgnoreCase("incotermsdescription"))
      return incotermsdescription;
    else if (fieldName.equalsIgnoreCase("isselected"))
      return isselected;
    else if (fieldName.equalsIgnoreCase("isdelivered"))
      return isdelivered;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("dropship_location_id") || fieldName.equals("dropshipLocationId"))
      return dropshipLocationId;
    else if (fieldName.equalsIgnoreCase("isinvoiced"))
      return isinvoiced;
    else if (fieldName.equalsIgnoreCase("posted"))
      return posted;
    else if (fieldName.equalsIgnoreCase("c_order_id") || fieldName.equals("cOrderId"))
      return cOrderId;
    else if (fieldName.equalsIgnoreCase("dateprinted"))
      return dateprinted;
    else if (fieldName.equalsIgnoreCase("dropship_bpartner_id") || fieldName.equals("dropshipBpartnerId"))
      return dropshipBpartnerId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Header6FE781F956E043CBAE78C9CA024881EBData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Header6FE781F956E043CBAE78C9CA024881EBData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(C_Order.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = C_Order.CreatedBy) as CreatedByR, " +
      "        to_char(C_Order.Updated, ?) as updated, " +
      "        to_char(C_Order.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        C_Order.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = C_Order.UpdatedBy) as UpdatedByR," +
      "        C_Order.AD_Org_ID, " +
      "(CASE WHEN C_Order.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "C_Order.C_DocTypeTarget_ID, " +
      "(CASE WHEN C_Order.C_DocTypeTarget_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))),'') ) END) AS C_DocTypeTarget_IDR, " +
      "C_Order.C_Return_Reason_ID, " +
      "C_Order.DocumentNo, " +
      "C_Order.DateOrdered, " +
      "C_Order.C_BPartner_ID, " +
      "(CASE WHEN C_Order.C_BPartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS C_BPartner_IDR, " +
      "C_Order.C_BPartner_Location_ID, " +
      "(CASE WHEN C_Order.C_BPartner_Location_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.Name), ''))),'') ) END) AS C_BPartner_Location_IDR, " +
      "C_Order.RM_PickFromShipment, " +
      "C_Order.M_PriceList_ID, " +
      "(CASE WHEN C_Order.M_PriceList_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table6.Name), ''))),'') ) END) AS M_PriceList_IDR, " +
      "C_Order.RM_ReceiveMaterials, " +
      "C_Order.DatePromised, " +
      "C_Order.FIN_Paymentmethod_ID, " +
      "(CASE WHEN C_Order.FIN_Paymentmethod_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table7.Name), ''))),'') ) END) AS FIN_Paymentmethod_IDR, " +
      "C_Order.RM_CreateInvoice, " +
      "C_Order.C_PaymentTerm_ID, " +
      "(CASE WHEN C_Order.C_PaymentTerm_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL8.Name IS NULL THEN TO_CHAR(table8.Name) ELSE TO_CHAR(tableTRL8.Name) END)), ''))),'') ) END) AS C_PaymentTerm_IDR, " +
      "C_Order.M_Warehouse_ID, " +
      "C_Order.InvoiceRule, " +
      "(CASE WHEN C_Order.InvoiceRule IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS InvoiceRuleR, " +
      "C_Order.DeliveryViaRule, " +
      "(CASE WHEN C_Order.DeliveryViaRule IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS DeliveryViaRuleR, " +
      "C_Order.M_Shipper_ID, " +
      "(CASE WHEN C_Order.M_Shipper_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table10.Name), ''))),'') ) END) AS M_Shipper_IDR, " +
      "C_Order.Description, " +
      "C_Order.EM_Prol_Cat_Pro_ID, " +
      "(CASE WHEN C_Order.EM_Prol_Cat_Pro_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL11.Name IS NULL THEN TO_CHAR(table11.Name) ELSE TO_CHAR(tableTRL11.Name) END)), ''))),'') ) END) AS EM_Prol_Cat_Pro_IDR, " +
      "C_Order.DocStatus, " +
      "C_Order.GrandTotal, " +
      "C_Order.TotalLines, " +
      "C_Order.C_Currency_ID, " +
      "C_Order.EM_Prol_Estado_Pedido, " +
      "(CASE WHEN C_Order.EM_Prol_Estado_Pedido IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list3.name),'') ) END) AS EM_Prol_Estado_PedidoR, " +
      "C_Order.Deliverynotes, " +
      "C_Order.EM_Prol_Sector_Actividad_ID, " +
      "COALESCE(C_Order.EM_Prol_Broker, 'N') AS EM_Prol_Broker, " +
      "C_Order.EM_Prol_Catalogo_Brokers_ID, " +
      "C_Order.EM_Prol_Fecha_Suscripcion, " +
      "COALESCE(C_Order.EM_Prol_Directo, 'N') AS EM_Prol_Directo, " +
      "C_Order.AD_User_ID, " +
      "C_Order.SalesRep_ID, " +
      "C_Order.C_DocType_ID, " +
      "C_Order.SO_Res_Status, " +
      "C_Order.POReference, " +
      "C_Order.BillTo_ID, " +
      "C_Order.Delivery_Location_ID, " +
      "C_Order.EM_APRM_AddPayment, " +
      "C_Order.DocAction, " +
      "C_Order.CopyFrom, " +
      "C_Order.CopyFromPO, " +
      "C_Order.DeliveryRule, " +
      "C_Order.FreightCostRule, " +
      "C_Order.FreightAmt, " +
      "COALESCE(C_Order.IsDiscountPrinted, 'N') AS IsDiscountPrinted, " +
      "C_Order.PriorityRule, " +
      "C_Order.C_Campaign_ID, " +
      "C_Order.ChargeAmt, " +
      "C_Order.C_Charge_ID, " +
      "C_Order.C_Activity_ID, " +
      "C_Order.AD_OrgTrx_ID, " +
      "C_Order.Quotation_ID, " +
      "C_Order.Calculate_Promotions, " +
      "C_Order.RM_AddOrphanLine, " +
      "C_Order.Convertquotation, " +
      "C_Order.C_Reject_Reason_ID, " +
      "C_Order.validuntil, " +
      "C_Order.C_Project_ID, " +
      "(CASE WHEN C_Order.C_Project_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table13.Value), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table13.Name), ''))),'') ) END) AS C_Project_IDR, " +
      "C_Order.C_Costcenter_ID, " +
      "C_Order.A_Asset_ID, " +
      "C_Order.User1_ID, " +
      "C_Order.User2_ID, " +
      "C_Order.EM_Prol_Fecha_Ini_Poliza, " +
      "C_Order.EM_Prol_Fecha_Fin_Poliza, " +
      "C_Order.EM_Prol_Num_Poliza, " +
      "C_Order.EM_Prol_Sum_Asegurada, " +
      "C_Order.EM_Prol_Enviar_Pedido, " +
      "C_Order.EM_Prol_Procesar, " +
      "list4.name as EM_Prol_Procesar_BTN, " +
      "C_Order.EM_Prol_Validar, " +
      "C_Order.DateAcct, " +
      "C_Order.Processing, " +
      "C_Order.AD_Client_ID, " +
      "COALESCE(C_Order.IsSOTrx, 'N') AS IsSOTrx, " +
      "C_Order.DropShip_User_ID, " +
      "C_Order.C_Incoterms_ID, " +
      "COALESCE(C_Order.IsActive, 'N') AS IsActive, " +
      "COALESCE(C_Order.IsTaxIncluded, 'N') AS IsTaxIncluded, " +
      "COALESCE(C_Order.IsPrinted, 'N') AS IsPrinted, " +
      "C_Order.Generatetemplate, " +
      "COALESCE(C_Order.IsSelfService, 'N') AS IsSelfService, " +
      "C_Order.PaymentRule, " +
      "C_Order.Incotermsdescription, " +
      "COALESCE(C_Order.IsSelected, 'N') AS IsSelected, " +
      "COALESCE(C_Order.IsDelivered, 'N') AS IsDelivered, " +
      "COALESCE(C_Order.Processed, 'N') AS Processed, " +
      "C_Order.DropShip_Location_ID, " +
      "COALESCE(C_Order.IsInvoiced, 'N') AS IsInvoiced, " +
      "C_Order.Posted, " +
      "C_Order.C_Order_ID, " +
      "C_Order.DatePrinted, " +
      "C_Order.DropShip_BPartner_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM C_Order left join (select AD_Org_ID, Name from AD_Org) table1 on (C_Order.AD_Org_ID = table1.AD_Org_ID) left join (select C_DocType_ID, Name from C_DocType) table2 on (C_Order.C_DocTypeTarget_ID =  table2.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL2 on (table2.C_DocType_ID = tableTRL2.C_DocType_ID and tableTRL2.AD_Language = ?)  left join (select C_BPartner_ID, Name from C_BPartner) table4 on (C_Order.C_BPartner_ID = table4.C_BPartner_ID) left join (select C_BPartner_Location_ID, Name from C_BPartner_Location) table5 on (C_Order.C_BPartner_Location_ID = table5.C_BPartner_Location_ID) left join (select M_PriceList_ID, Name from M_PriceList) table6 on (C_Order.M_PriceList_ID = table6.M_PriceList_ID) left join (select FIN_Paymentmethod_ID, Name from FIN_Paymentmethod) table7 on (C_Order.FIN_Paymentmethod_ID = table7.FIN_Paymentmethod_ID) left join (select C_PaymentTerm_ID, Name from C_PaymentTerm) table8 on (C_Order.C_PaymentTerm_ID = table8.C_PaymentTerm_ID) left join (select C_PaymentTerm_ID,AD_Language, Name from C_PaymentTerm_TRL) tableTRL8 on (table8.C_PaymentTerm_ID = tableTRL8.C_PaymentTerm_ID and tableTRL8.AD_Language = ?)  left join ad_ref_list_v list1 on (C_Order.InvoiceRule = list1.value and list1.ad_reference_id = '150' and list1.ad_language = ?)  left join ad_ref_list_v list2 on (C_Order.DeliveryViaRule = list2.value and list2.ad_reference_id = '152' and list2.ad_language = ?)  left join (select M_Shipper_ID, Name from M_Shipper) table10 on (C_Order.M_Shipper_ID = table10.M_Shipper_ID) left join (select M_Product_Category_ID, value, Name from M_Product_Category) table11 on (C_Order.EM_Prol_Cat_Pro_ID =  table11.M_Product_Category_ID) left join (select M_Product_Category_ID,AD_Language, Name from M_Product_Category_TRL) tableTRL11 on (table11.M_Product_Category_ID = tableTRL11.M_Product_Category_ID and tableTRL11.AD_Language = ?)  left join ad_ref_list_v list3 on (C_Order.EM_Prol_Estado_Pedido = list3.value and list3.ad_reference_id = '02BC73ABA84748BE822FBFCDB619A338' and list3.ad_language = ?)  left join (select C_Project_ID, Value, Name from C_Project) table13 on (C_Order.C_Project_ID = table13.C_Project_ID) left join ad_ref_list_v list4 on (list4.ad_reference_id = '93300A8E83CD4C5CA44C5F5571F207E8' and list4.ad_language = ?  AND C_Order.EM_Prol_Procesar = TO_CHAR(list4.value))" +
      "        WHERE 2=2 " +
      " AND C_Order.IsSOTrx='Y'" +
      "        AND 1=1 " +
      "        AND C_Order.C_Order_ID = ? " +
      "        AND C_Order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND C_Order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Header6FE781F956E043CBAE78C9CA024881EBData objectHeader6FE781F956E043CBAE78C9CA024881EBData = new Header6FE781F956E043CBAE78C9CA024881EBData();
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.created = UtilSql.getValue(result, "created");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.updated = UtilSql.getValue(result, "updated");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.updatedby = UtilSql.getValue(result, "updatedby");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cDoctypetargetId = UtilSql.getValue(result, "c_doctypetarget_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cDoctypetargetIdr = UtilSql.getValue(result, "c_doctypetarget_idr");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cReturnReasonId = UtilSql.getValue(result, "c_return_reason_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.documentno = UtilSql.getValue(result, "documentno");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.dateordered = UtilSql.getDateValue(result, "dateordered", "dd-MM-yyyy");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cBpartnerLocationId = UtilSql.getValue(result, "c_bpartner_location_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cBpartnerLocationIdr = UtilSql.getValue(result, "c_bpartner_location_idr");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.rmPickfromshipment = UtilSql.getValue(result, "rm_pickfromshipment");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.mPricelistId = UtilSql.getValue(result, "m_pricelist_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.mPricelistIdr = UtilSql.getValue(result, "m_pricelist_idr");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.rmReceivematerials = UtilSql.getValue(result, "rm_receivematerials");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.datepromised = UtilSql.getDateValue(result, "datepromised", "dd-MM-yyyy");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.finPaymentmethodId = UtilSql.getValue(result, "fin_paymentmethod_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.finPaymentmethodIdr = UtilSql.getValue(result, "fin_paymentmethod_idr");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.rmCreateinvoice = UtilSql.getValue(result, "rm_createinvoice");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cPaymenttermId = UtilSql.getValue(result, "c_paymentterm_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cPaymenttermIdr = UtilSql.getValue(result, "c_paymentterm_idr");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.mWarehouseId = UtilSql.getValue(result, "m_warehouse_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.invoicerule = UtilSql.getValue(result, "invoicerule");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.invoiceruler = UtilSql.getValue(result, "invoiceruler");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.deliveryviarule = UtilSql.getValue(result, "deliveryviarule");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.deliveryviaruler = UtilSql.getValue(result, "deliveryviaruler");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.mShipperId = UtilSql.getValue(result, "m_shipper_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.mShipperIdr = UtilSql.getValue(result, "m_shipper_idr");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.description = UtilSql.getValue(result, "description");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.emProlCatProId = UtilSql.getValue(result, "em_prol_cat_pro_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.emProlCatProIdr = UtilSql.getValue(result, "em_prol_cat_pro_idr");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.docstatus = UtilSql.getValue(result, "docstatus");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.grandtotal = UtilSql.getValue(result, "grandtotal");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.totallines = UtilSql.getValue(result, "totallines");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cCurrencyId = UtilSql.getValue(result, "c_currency_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.emProlEstadoPedido = UtilSql.getValue(result, "em_prol_estado_pedido");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.emProlEstadoPedidor = UtilSql.getValue(result, "em_prol_estado_pedidor");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.deliverynotes = UtilSql.getValue(result, "deliverynotes");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.emProlSectorActividadId = UtilSql.getValue(result, "em_prol_sector_actividad_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.emProlBroker = UtilSql.getValue(result, "em_prol_broker");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.emProlCatalogoBrokersId = UtilSql.getValue(result, "em_prol_catalogo_brokers_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.emProlFechaSuscripcion = UtilSql.getDateValue(result, "em_prol_fecha_suscripcion", "dd-MM-yyyy");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.emProlDirecto = UtilSql.getValue(result, "em_prol_directo");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.adUserId = UtilSql.getValue(result, "ad_user_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.salesrepId = UtilSql.getValue(result, "salesrep_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.soResStatus = UtilSql.getValue(result, "so_res_status");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.poreference = UtilSql.getValue(result, "poreference");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.billtoId = UtilSql.getValue(result, "billto_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.deliveryLocationId = UtilSql.getValue(result, "delivery_location_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.emAprmAddpayment = UtilSql.getValue(result, "em_aprm_addpayment");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.docaction = UtilSql.getValue(result, "docaction");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.copyfrom = UtilSql.getValue(result, "copyfrom");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.copyfrompo = UtilSql.getValue(result, "copyfrompo");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.deliveryrule = UtilSql.getValue(result, "deliveryrule");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.freightcostrule = UtilSql.getValue(result, "freightcostrule");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.freightamt = UtilSql.getValue(result, "freightamt");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.isdiscountprinted = UtilSql.getValue(result, "isdiscountprinted");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.priorityrule = UtilSql.getValue(result, "priorityrule");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cCampaignId = UtilSql.getValue(result, "c_campaign_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.chargeamt = UtilSql.getValue(result, "chargeamt");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cChargeId = UtilSql.getValue(result, "c_charge_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cActivityId = UtilSql.getValue(result, "c_activity_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.adOrgtrxId = UtilSql.getValue(result, "ad_orgtrx_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.quotationId = UtilSql.getValue(result, "quotation_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.calculatePromotions = UtilSql.getValue(result, "calculate_promotions");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.rmAddorphanline = UtilSql.getValue(result, "rm_addorphanline");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.convertquotation = UtilSql.getValue(result, "convertquotation");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cRejectReasonId = UtilSql.getValue(result, "c_reject_reason_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.validuntil = UtilSql.getDateValue(result, "validuntil", "dd-MM-yyyy");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cProjectId = UtilSql.getValue(result, "c_project_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cProjectIdr = UtilSql.getValue(result, "c_project_idr");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cCostcenterId = UtilSql.getValue(result, "c_costcenter_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.aAssetId = UtilSql.getValue(result, "a_asset_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.user1Id = UtilSql.getValue(result, "user1_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.user2Id = UtilSql.getValue(result, "user2_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.emProlFechaIniPoliza = UtilSql.getDateValue(result, "em_prol_fecha_ini_poliza", "dd-MM-yyyy");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.emProlFechaFinPoliza = UtilSql.getDateValue(result, "em_prol_fecha_fin_poliza", "dd-MM-yyyy");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.emProlNumPoliza = UtilSql.getValue(result, "em_prol_num_poliza");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.emProlSumAsegurada = UtilSql.getValue(result, "em_prol_sum_asegurada");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.emProlEnviarPedido = UtilSql.getValue(result, "em_prol_enviar_pedido");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.emProlProcesar = UtilSql.getValue(result, "em_prol_procesar");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.emProlProcesarBtn = UtilSql.getValue(result, "em_prol_procesar_btn");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.emProlValidar = UtilSql.getValue(result, "em_prol_validar");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.dateacct = UtilSql.getDateValue(result, "dateacct", "dd-MM-yyyy");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.processing = UtilSql.getValue(result, "processing");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.issotrx = UtilSql.getValue(result, "issotrx");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.dropshipUserId = UtilSql.getValue(result, "dropship_user_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cIncotermsId = UtilSql.getValue(result, "c_incoterms_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.isactive = UtilSql.getValue(result, "isactive");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.istaxincluded = UtilSql.getValue(result, "istaxincluded");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.isprinted = UtilSql.getValue(result, "isprinted");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.generatetemplate = UtilSql.getValue(result, "generatetemplate");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.isselfservice = UtilSql.getValue(result, "isselfservice");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.paymentrule = UtilSql.getValue(result, "paymentrule");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.incotermsdescription = UtilSql.getValue(result, "incotermsdescription");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.isselected = UtilSql.getValue(result, "isselected");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.isdelivered = UtilSql.getValue(result, "isdelivered");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.processed = UtilSql.getValue(result, "processed");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.dropshipLocationId = UtilSql.getValue(result, "dropship_location_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.isinvoiced = UtilSql.getValue(result, "isinvoiced");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.posted = UtilSql.getValue(result, "posted");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.cOrderId = UtilSql.getValue(result, "c_order_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.dateprinted = UtilSql.getDateValue(result, "dateprinted", "dd-MM-yyyy");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.dropshipBpartnerId = UtilSql.getValue(result, "dropship_bpartner_id");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.language = UtilSql.getValue(result, "language");
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.adUserClient = "";
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.adOrgClient = "";
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.createdby = "";
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.trBgcolor = "";
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.totalCount = "";
        objectHeader6FE781F956E043CBAE78C9CA024881EBData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectHeader6FE781F956E043CBAE78C9CA024881EBData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Header6FE781F956E043CBAE78C9CA024881EBData objectHeader6FE781F956E043CBAE78C9CA024881EBData[] = new Header6FE781F956E043CBAE78C9CA024881EBData[vector.size()];
    vector.copyInto(objectHeader6FE781F956E043CBAE78C9CA024881EBData);
    return(objectHeader6FE781F956E043CBAE78C9CA024881EBData);
  }

/**
Create a registry
 */
  public static Header6FE781F956E043CBAE78C9CA024881EBData[] set(String emProlValidar, String convertquotation, String validuntil, String aAssetId, String cOrderId, String adClientId, String adOrgId, String isactive, String createdby, String createdbyr, String updatedby, String updatedbyr, String documentno, String docstatus, String docaction, String cDoctypeId, String cDoctypetargetId, String description, String isdelivered, String isinvoiced, String isprinted, String dateordered, String datepromised, String dateacct, String salesrepId, String cPaymenttermId, String billtoId, String cCurrencyId, String invoicerule, String freightamt, String deliveryviarule, String mShipperId, String priorityrule, String totallines, String grandtotal, String mWarehouseId, String mPricelistId, String emProlFechaIniPoliza, String processing, String cCampaignId, String emProlEstadoPedido, String emProlCatalogoBrokersId, String cBpartnerId, String cBpartnerIdr, String adUserId, String poreference, String cChargeId, String chargeamt, String processed, String cBpartnerLocationId, String cProjectId, String cProjectIdr, String cActivityId, String emProlSectorActividadId, String quotationId, String issotrx, String dateprinted, String deliveryrule, String freightcostrule, String paymentrule, String isdiscountprinted, String posted, String istaxincluded, String isselected, String emProlBroker, String emProlNumPoliza, String cCostcenterId, String emProlDirecto, String deliverynotes, String cIncotermsId, String incotermsdescription, String generatetemplate, String deliveryLocationId, String copyfrompo, String finPaymentmethodId, String dropshipUserId, String dropshipBpartnerId, String copyfrom, String dropshipLocationId, String isselfservice, String emProlFechaFinPoliza, String adOrgtrxId, String user2Id, String user1Id, String calculatePromotions, String emProlFechaSuscripcion, String emProlSumAsegurada, String emProlProcesar, String emProlProcesarBtn, String rmPickfromshipment, String rmReceivematerials, String rmCreateinvoice, String cReturnReasonId, String rmAddorphanline, String soResStatus, String cRejectReasonId, String emProlCatProId, String emProlEnviarPedido, String emAprmAddpayment)    throws ServletException {
    Header6FE781F956E043CBAE78C9CA024881EBData objectHeader6FE781F956E043CBAE78C9CA024881EBData[] = new Header6FE781F956E043CBAE78C9CA024881EBData[1];
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0] = new Header6FE781F956E043CBAE78C9CA024881EBData();
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].created = "";
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].createdbyr = createdbyr;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].updated = "";
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].updatedTimeStamp = "";
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].updatedby = updatedby;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].updatedbyr = updatedbyr;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].adOrgId = adOrgId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].adOrgIdr = "";
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cDoctypetargetId = cDoctypetargetId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cDoctypetargetIdr = "";
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cReturnReasonId = cReturnReasonId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].documentno = documentno;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].dateordered = dateordered;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cBpartnerId = cBpartnerId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cBpartnerIdr = cBpartnerIdr;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cBpartnerLocationId = cBpartnerLocationId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cBpartnerLocationIdr = "";
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].rmPickfromshipment = rmPickfromshipment;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].mPricelistId = mPricelistId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].mPricelistIdr = "";
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].rmReceivematerials = rmReceivematerials;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].datepromised = datepromised;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].finPaymentmethodId = finPaymentmethodId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].finPaymentmethodIdr = "";
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].rmCreateinvoice = rmCreateinvoice;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cPaymenttermId = cPaymenttermId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cPaymenttermIdr = "";
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].mWarehouseId = mWarehouseId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].invoicerule = invoicerule;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].invoiceruler = "";
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].deliveryviarule = deliveryviarule;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].deliveryviaruler = "";
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].mShipperId = mShipperId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].mShipperIdr = "";
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].description = description;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].emProlCatProId = emProlCatProId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].emProlCatProIdr = "";
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].docstatus = docstatus;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].grandtotal = grandtotal;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].totallines = totallines;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cCurrencyId = cCurrencyId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].emProlEstadoPedido = emProlEstadoPedido;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].emProlEstadoPedidor = "";
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].deliverynotes = deliverynotes;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].emProlSectorActividadId = emProlSectorActividadId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].emProlBroker = emProlBroker;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].emProlCatalogoBrokersId = emProlCatalogoBrokersId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].emProlFechaSuscripcion = emProlFechaSuscripcion;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].emProlDirecto = emProlDirecto;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].adUserId = adUserId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].salesrepId = salesrepId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cDoctypeId = cDoctypeId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].soResStatus = soResStatus;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].poreference = poreference;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].billtoId = billtoId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].deliveryLocationId = deliveryLocationId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].emAprmAddpayment = emAprmAddpayment;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].docaction = docaction;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].copyfrom = copyfrom;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].copyfrompo = copyfrompo;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].deliveryrule = deliveryrule;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].freightcostrule = freightcostrule;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].freightamt = freightamt;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].isdiscountprinted = isdiscountprinted;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].priorityrule = priorityrule;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cCampaignId = cCampaignId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].chargeamt = chargeamt;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cChargeId = cChargeId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cActivityId = cActivityId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].adOrgtrxId = adOrgtrxId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].quotationId = quotationId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].calculatePromotions = calculatePromotions;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].rmAddorphanline = rmAddorphanline;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].convertquotation = convertquotation;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cRejectReasonId = cRejectReasonId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].validuntil = validuntil;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cProjectId = cProjectId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cProjectIdr = cProjectIdr;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cCostcenterId = cCostcenterId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].aAssetId = aAssetId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].user1Id = user1Id;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].user2Id = user2Id;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].emProlFechaIniPoliza = emProlFechaIniPoliza;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].emProlFechaFinPoliza = emProlFechaFinPoliza;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].emProlNumPoliza = emProlNumPoliza;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].emProlSumAsegurada = emProlSumAsegurada;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].emProlEnviarPedido = emProlEnviarPedido;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].emProlProcesar = emProlProcesar;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].emProlProcesarBtn = emProlProcesarBtn;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].emProlValidar = emProlValidar;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].dateacct = dateacct;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].processing = processing;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].adClientId = adClientId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].issotrx = issotrx;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].dropshipUserId = dropshipUserId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cIncotermsId = cIncotermsId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].isactive = isactive;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].istaxincluded = istaxincluded;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].isprinted = isprinted;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].generatetemplate = generatetemplate;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].isselfservice = isselfservice;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].paymentrule = paymentrule;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].incotermsdescription = incotermsdescription;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].isselected = isselected;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].isdelivered = isdelivered;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].processed = processed;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].dropshipLocationId = dropshipLocationId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].isinvoiced = isinvoiced;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].posted = posted;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].cOrderId = cOrderId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].dateprinted = dateprinted;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].dropshipBpartnerId = dropshipBpartnerId;
    objectHeader6FE781F956E043CBAE78C9CA024881EBData[0].language = "";
    return objectHeader6FE781F956E043CBAE78C9CA024881EBData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef2166_0(ConnectionProvider connectionProvider, String CreatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as CreatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2168_1(ConnectionProvider connectionProvider, String UpdatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as UpdatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2762_2(ConnectionProvider connectionProvider, String C_BPartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as C_BPartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_BPartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef3402_3(ConnectionProvider connectionProvider, String C_Project_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Value), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as C_Project_ID FROM C_Project left join (select C_Project_ID, Value, Name from C_Project) table2 on (C_Project.C_Project_ID = table2.C_Project_ID) WHERE C_Project.isActive='Y' AND C_Project.C_Project_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Project_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_project_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for action search
 */
  public static String selectActDefM_AttributeSetInstance_ID(ConnectionProvider connectionProvider, String M_AttributeSetInstance_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT Description FROM M_AttributeSetInstance WHERE isActive='Y' AND M_AttributeSetInstance_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, M_AttributeSetInstance_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "description");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE C_Order" +
      "        SET AD_Org_ID = (?) , C_DocTypeTarget_ID = (?) , C_Return_Reason_ID = (?) , DocumentNo = (?) , DateOrdered = TO_DATE(?) , C_BPartner_ID = (?) , C_BPartner_Location_ID = (?) , RM_PickFromShipment = (?) , M_PriceList_ID = (?) , RM_ReceiveMaterials = (?) , DatePromised = TO_DATE(?) , FIN_Paymentmethod_ID = (?) , RM_CreateInvoice = (?) , C_PaymentTerm_ID = (?) , M_Warehouse_ID = (?) , InvoiceRule = (?) , DeliveryViaRule = (?) , M_Shipper_ID = (?) , Description = (?) , EM_Prol_Cat_Pro_ID = (?) , DocStatus = (?) , GrandTotal = TO_NUMBER(?) , TotalLines = TO_NUMBER(?) , C_Currency_ID = (?) , EM_Prol_Estado_Pedido = (?) , Deliverynotes = (?) , EM_Prol_Sector_Actividad_ID = (?) , EM_Prol_Broker = (?) , EM_Prol_Catalogo_Brokers_ID = (?) , EM_Prol_Fecha_Suscripcion = TO_DATE(?) , EM_Prol_Directo = (?) , AD_User_ID = (?) , SalesRep_ID = (?) , C_DocType_ID = (?) , SO_Res_Status = (?) , POReference = (?) , BillTo_ID = (?) , Delivery_Location_ID = (?) , EM_APRM_AddPayment = (?) , DocAction = (?) , CopyFrom = (?) , CopyFromPO = (?) , DeliveryRule = (?) , FreightCostRule = (?) , FreightAmt = TO_NUMBER(?) , IsDiscountPrinted = (?) , PriorityRule = (?) , C_Campaign_ID = (?) , ChargeAmt = TO_NUMBER(?) , C_Charge_ID = (?) , C_Activity_ID = (?) , AD_OrgTrx_ID = (?) , Quotation_ID = (?) , Calculate_Promotions = (?) , RM_AddOrphanLine = (?) , Convertquotation = (?) , C_Reject_Reason_ID = (?) , validuntil = TO_DATE(?) , C_Project_ID = (?) , C_Costcenter_ID = (?) , A_Asset_ID = (?) , User1_ID = (?) , User2_ID = (?) , EM_Prol_Fecha_Ini_Poliza = TO_DATE(?) , EM_Prol_Fecha_Fin_Poliza = TO_DATE(?) , EM_Prol_Num_Poliza = (?) , EM_Prol_Sum_Asegurada = TO_NUMBER(?) , EM_Prol_Enviar_Pedido = (?) , EM_Prol_Procesar = (?) , EM_Prol_Validar = (?) , DateAcct = TO_DATE(?) , Processing = (?) , AD_Client_ID = (?) , IsSOTrx = (?) , DropShip_User_ID = (?) , C_Incoterms_ID = (?) , IsActive = (?) , IsTaxIncluded = (?) , IsPrinted = (?) , Generatetemplate = (?) , IsSelfService = (?) , PaymentRule = (?) , Incotermsdescription = (?) , IsSelected = (?) , IsDelivered = (?) , Processed = (?) , DropShip_Location_ID = (?) , IsInvoiced = (?) , Posted = (?) , C_Order_ID = (?) , DatePrinted = TO_DATE(?) , DropShip_BPartner_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE C_Order.C_Order_ID = ? " +
      "        AND C_Order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND C_Order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypetargetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cReturnReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmPickfromshipment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmReceivematerials);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datepromised);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmCreateinvoice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPaymenttermId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, invoicerule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryviarule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mShipperId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlCatProId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grandtotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totallines);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlEstadoPedido);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliverynotes);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlSectorActividadId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlBroker);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlCatalogoBrokersId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlFechaSuscripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlDirecto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesrepId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, soResStatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, poreference);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, billtoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAprmAddpayment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrompo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightcostrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdiscountprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, priorityrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCampaignId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, chargeamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cChargeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cActivityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgtrxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quotationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calculatePromotions);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmAddorphanline);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, convertquotation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cRejectReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, validuntil);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCostcenterId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aAssetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlFechaIniPoliza);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlFechaFinPoliza);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlNumPoliza);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlSumAsegurada);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlEnviarPedido);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlProcesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlValidar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issotrx);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, istaxincluded);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generatetemplate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselfservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paymentrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, incotermsdescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselected);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdelivered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO C_Order " +
      "        (AD_Org_ID, C_DocTypeTarget_ID, C_Return_Reason_ID, DocumentNo, DateOrdered, C_BPartner_ID, C_BPartner_Location_ID, RM_PickFromShipment, M_PriceList_ID, RM_ReceiveMaterials, DatePromised, FIN_Paymentmethod_ID, RM_CreateInvoice, C_PaymentTerm_ID, M_Warehouse_ID, InvoiceRule, DeliveryViaRule, M_Shipper_ID, Description, EM_Prol_Cat_Pro_ID, DocStatus, GrandTotal, TotalLines, C_Currency_ID, EM_Prol_Estado_Pedido, Deliverynotes, EM_Prol_Sector_Actividad_ID, EM_Prol_Broker, EM_Prol_Catalogo_Brokers_ID, EM_Prol_Fecha_Suscripcion, EM_Prol_Directo, AD_User_ID, SalesRep_ID, C_DocType_ID, SO_Res_Status, POReference, BillTo_ID, Delivery_Location_ID, EM_APRM_AddPayment, DocAction, CopyFrom, CopyFromPO, DeliveryRule, FreightCostRule, FreightAmt, IsDiscountPrinted, PriorityRule, C_Campaign_ID, ChargeAmt, C_Charge_ID, C_Activity_ID, AD_OrgTrx_ID, Quotation_ID, Calculate_Promotions, RM_AddOrphanLine, Convertquotation, C_Reject_Reason_ID, validuntil, C_Project_ID, C_Costcenter_ID, A_Asset_ID, User1_ID, User2_ID, EM_Prol_Fecha_Ini_Poliza, EM_Prol_Fecha_Fin_Poliza, EM_Prol_Num_Poliza, EM_Prol_Sum_Asegurada, EM_Prol_Enviar_Pedido, EM_Prol_Procesar, EM_Prol_Validar, DateAcct, Processing, AD_Client_ID, IsSOTrx, DropShip_User_ID, C_Incoterms_ID, IsActive, IsTaxIncluded, IsPrinted, Generatetemplate, IsSelfService, PaymentRule, Incotermsdescription, IsSelected, IsDelivered, Processed, DropShip_Location_ID, IsInvoiced, Posted, C_Order_ID, DatePrinted, DropShip_BPartner_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), TO_DATE(?), TO_DATE(?), (?), TO_NUMBER(?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypetargetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cReturnReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmPickfromshipment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmReceivematerials);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datepromised);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmCreateinvoice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPaymenttermId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, invoicerule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryviarule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mShipperId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlCatProId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grandtotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totallines);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlEstadoPedido);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliverynotes);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlSectorActividadId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlBroker);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlCatalogoBrokersId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlFechaSuscripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlDirecto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesrepId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, soResStatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, poreference);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, billtoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAprmAddpayment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrompo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightcostrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdiscountprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, priorityrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCampaignId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, chargeamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cChargeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cActivityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgtrxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quotationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calculatePromotions);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmAddorphanline);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, convertquotation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cRejectReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, validuntil);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCostcenterId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aAssetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlFechaIniPoliza);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlFechaFinPoliza);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlNumPoliza);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlSumAsegurada);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlEnviarPedido);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlProcesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emProlValidar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issotrx);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, istaxincluded);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generatetemplate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselfservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paymentrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, incotermsdescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselected);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdelivered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM C_Order" +
      "        WHERE C_Order.C_Order_ID = ? " +
      "        AND C_Order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND C_Order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM C_Order" +
      "         WHERE C_Order.C_Order_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM C_Order" +
      "         WHERE C_Order.C_Order_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
